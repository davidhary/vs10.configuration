﻿Imports System

Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports isr.Configuration.Profile



'''<summary>
'''This is a test class for ScriberTest and is intended
'''to contain all ScriberTest Unit Tests
'''</summary>
<TestClass()> 
Public Class ScriberTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize()>  
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    Private Const sectionName As String = "Unit Tests"
    '''<summary>
    '''A test for ReadBoolean
    '''</summary>
    <TestMethod()> 
    Public Sub ReadBooleanTest()
        Dim target As Scriber = New Scriber()
        Dim defaultValue As Boolean = False
        Dim expected As Boolean = False
        Dim actual As Boolean
        actual = target.ReadBoolean(defaultValue)
        Assert.AreEqual(expected, actual)

        Dim ini As New isr.Configuration.Profile.Scriber()
        ini.SectionName = sectionName
        ini.SettingName = "Boolean"
        expected = True
        Dim message As String = "Profile.Scriber.Write/Read Boolean: Expected {0} ?= {1}"

        ' write expected to section 
        ini.Write(expected)

        actual = ini.ReadBoolean(False)
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, expected, actual)
        ' Debug.WriteLine(ini.FilePathName)
        Debug.WriteLine(message)
        Assert.AreEqual(expected, actual, message)
    End Sub

    '''<summary>
    '''A test for ReadDateTime
    '''</summary>
    <TestMethod()> 
    Public Sub ReadDateTimeTest()
        Dim target As Scriber = New Scriber()
        Dim defaultValue As DateTime = DateTime.MinValue
        Dim expected As DateTime = New DateTime()
        Dim actual As DateTime

        Dim ini As New isr.Configuration.Profile.Scriber()
        ini.SectionName = sectionName
        ini.SettingName = "DateTime"
        expected = DateTime.Now
        Dim message As String = "Profile.Scriber.Write/Read Date: Expected {0} ?= {1}"

        ' write expected to section 
        ini.Write(expected)

        actual = ini.ReadDateTime(DateTime.MinValue)
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, expected, actual)
        ' Debug.WriteLine(ini.FilePathName)
        Debug.WriteLine(message)
        actual = target.ReadDateTime(defaultValue)
        Assert.AreEqual(expected, actual)
    End Sub


    '''<summary>
    '''A test for Read OA Date
    '''</summary>
    <TestMethod()> 
    Public Sub ReadOADateTest()
        Dim target As Scriber = New Scriber()
        Dim defaultValue As Double = 0.0!

        Dim ini As New isr.Configuration.Profile.Scriber()
        ini.SectionName = sectionName
        ini.SettingName = "OADate"
        Dim expected As DateTime = DateTime.Now
        Dim message As String = "Profile.Scriber.Write/Read OADate: Expected {0} ?= {1}"

        ' write expected to section 
        ini.Write(expected.ToOADate)

        Dim actual As DateTime = DateTime.FromOADate(ini.ReadDouble(DateTime.MinValue.ToOADate))
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, expected, actual)
        ' Debug.WriteLine(ini.FilePathName)
        Debug.WriteLine(message)
        ' note that the program failed comparing the dates
        Assert.AreEqual(expected.ToOADate, actual.ToOADate, message)

    End Sub

    '''<summary>
    '''A test for ReadDouble
    '''</summary>
    <TestMethod()> 
    Public Sub ReadDoubleTest()
        Dim target As Scriber = New Scriber() ' TO_DO: Initialize to an appropriate value
        Dim defaultValue As Double = 0.0!
        Dim expected As Double = 0.0!
        Dim actual As Double
        Dim ini As New isr.Configuration.Profile.Scriber()
        ini.SectionName = sectionName
        ini.SettingName = "Pi"
        expected = Math.PI
        Dim message As String = "Profile.Scriber.Write/Read Double: Expected {0} ?= {1}"

        ' write expected to section 
        ini.Write(expected)

        actual = ini.ReadDouble(0)
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, expected, actual)
        ' Debug.WriteLine(ini.FilePathName)
        Debug.WriteLine(message)
        ' note that the program failed comparing the dates
        Assert.AreEqual(expected, actual, 0.00000001, message)

    End Sub

    '''<summary>
    '''A test for ReadInt32
    '''</summary>
    <TestMethod()> 
    Public Sub ReadInt32Test()
        Dim target As Scriber = New Scriber()
        Dim ini As New isr.Configuration.Profile.Scriber()
        ini.SectionName = sectionName
        ini.SettingName = "integer"
        Dim expected As Int32 = 1234
        Dim message As String = "Profile.Scriber.Write/Read Int32: Expected {0} ?= {1}"

        ' write expected to section 
        ini.Write(expected)

        Dim actual As Integer = ini.ReadInt32(0)
        message = String.Format(Globalization.CultureInfo.CurrentCulture, message, expected, actual)
        Debug.WriteLine(message)
        Assert.AreEqual(expected, actual, message)
    End Sub
End Class
