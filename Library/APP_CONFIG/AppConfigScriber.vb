''' <summary> Manages configuration application settings. </summary>
''' <remarks> Reads and writes application settings to the assembly application configuration file.
''' Protects the configurations things. </remarks>
''' <license> (c) 2003 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="1/17/2014" by="David" revision=""> Created. </history>
Public Class AppConfigScriber
    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Initializes a new instance of the <see cref="AppConfigScriber" /> class. </summary>
    Public Sub New()
        Me.New(System.Windows.Forms.Application.ExecutablePath & ProfileScriber.DefaultExtension)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="AppConfigScriber" /> class. </summary>
    ''' <param name="filePath"> The file path. This also serves as the instance name. </param>
    Public Sub New(ByVal filePath As String)
        MyBase.New()
        Me._SystemType = System.Type.GetType("System.Int32")
        Me._FilePath = filePath
        Me._DefaultFileName = System.Windows.Forms.Application.ExecutablePath & ProfileScriber.DefaultExtension
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks> Do not make this method Overridable (virtual) because a derived class should not be
    ''' able to override this method. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks> Executes in two distinct scenarios as determined by its disposing parameter.  If True,
    ''' the method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed. </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    ''' False if this method releases only unmanaged resources. </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources
                Me._FilePath = ""
                Me._xmlDocument = Nothing
                Me._element = Nothing

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

    ''' <summary> This destructor will run only if the Dispose method does not get called. It gives the
    ''' base class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class. </summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " BASE METHODS AND PROPERTIES "

    ''' <summary> Validates the name of the file. </summary>
    ''' <param name="filePath"> The file path. </param>
    ''' <returns> <c>True</c> if file name is valid, <c>False</c> otherwise. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Shared Function ValidateFileName(ByVal filePath As String) As Boolean
        Try
            ' Check that the file name is legal.
            System.IO.File.Exists(filePath)
            Return True
        Catch
            Return False
        End Try
    End Function

    ''' <summary> Gets or sets the default name of the file. </summary>
    ''' <value> The default name of the file. </value>
    Public Property DefaultFileName As String

#End Region


#Region " SHARED "

    ''' <summary> Holds the default extension of an Application configuration file. </summary>
    Public Const DefaultExtension As String = ".config"

#End Region

#Region " TYPES "

    Private Const elementKeyXpathFormat As String = "add[@key='{0}']"
    Private Const nodeName As String = "appSettings"
    Private Const attributeName As String = "add"
    Private Const elementKeyName As String = "key"
    Private Const elementValueName As String = "value"

#End Region

#Region " SHARED "

    ''' <summary> Protect or un-protects the specified configuration section. </summary>
    ''' <param name="protect"> true to protect. </param>
    ''' <example> Turn on protection of connection strings.
    ''' <code>
    ''' AppConfig.ProtectConnectionStrings(True)
    ''' </code>
    ''' Turn off protection of connection strings.
    ''' <code>
    ''' AppConfig.ProtectConnectionStrings(False)
    ''' </code></example>
    ''' <history date="06/17/07" by="Dariush Tasdighi" revision="1.0.2807.x"> Code Project. </history>
    Public Shared Sub ProtectConnectionStrings(ByVal protect As Boolean)

        ' get the executable path.
        Dim path As String = System.Windows.Forms.Application.ExecutablePath

        ' Define the configuration provider name.
        Dim configProvider As String = "DataProtectionConfigurationProvider"
        ' string configProvider = "RSAProtectedConfigurationProvider";

        Dim assemblyConfig As System.Configuration.Configuration = Nothing
        Dim configSection As System.Configuration.ConnectionStringsSection = Nothing

        ' Open the configuration file and retrieve the connectionStrings section.

        ' For Web!
        ' assemblyConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");

        ' For Windows!
        ' Takes the executable file name without the config extension.
        assemblyConfig = System.Configuration.ConfigurationManager.OpenExeConfiguration(path)

        If assemblyConfig IsNot Nothing Then

            Dim isChanged As Boolean
            configSection = TryCast(assemblyConfig.GetSection("connectionStrings"), 
                System.Configuration.ConnectionStringsSection)

            If configSection IsNot Nothing Then
                If Not (configSection.ElementInformation.IsLocked OrElse configSection.SectionInformation.IsLocked) Then
                    If protect Then
                        If Not configSection.SectionInformation.IsProtected Then
                            isChanged = True

                            ' Encrypt the section.
                            configSection.SectionInformation.ProtectSection(configProvider)
                        End If
                    Else
                        If configSection.SectionInformation.IsProtected Then
                            isChanged = True

                            ' Remove encryption.
                            configSection.SectionInformation.UnprotectSection()
                        End If
                    End If
                End If

                If isChanged Then
                    ' Indicates whether the associated configuration section will be saved even if it has not been modified.
                    configSection.SectionInformation.ForceSave = True

                    ' Save the current configuration.
                    assemblyConfig.Save()
                End If
            End If
        End If
    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Gets or sets the opened status of the instance. </summary>
    ''' <value> <c>IsOpen</c> is a <see cref="Boolean"/> property that is True if the instance is
    ''' open. </value>
    Public Property IsOpen() As Boolean

    ''' <summary>Opens this instance.</summary>
    ''' <exception cref="OperationOpenException" guarantee="strong"></exception>
    Public Overridable Sub [Open]()

        If System.IO.File.Exists(Me.FilePath) Then
            Try
                Me._xmlDocument = New System.Xml.XmlDocument
                Me._xmlDocument.Load(Me.FilePath)
                Me._element = CType(Me._xmlDocument.SelectSingleNode("//configuration/appSettings"), System.Xml.XmlElement)
                If Me._element Is Nothing Then
                    Me._element = Me._xmlDocument.CreateElement(nodeName)
                    Me._xmlDocument.DocumentElement.AppendChild(Me._element)
                End If
            Catch ex As System.Xml.XmlException
                Throw New isr.Configuration.BaseException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                        "Unable to load the configuration file '{0}' for this application.",
                                                                        Me.FilePath),
                                                                    ex)
            End Try
        Else
            Throw New isr.Configuration.IOException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                   "Application '{0}' has no configuration file.",
                                                                   System.Windows.Forms.Application.ExecutablePath))
        End If
        Me.IsOpen = True

    End Sub

    ''' <summary> Opens the configuration file. </summary>
    ''' <remarks> Use this method to open the instance.  The method returns true if success or false if
    ''' it failed opening the instance. </remarks>
    ''' <exception cref="OperationOpenException" guarantee="strong"> . </exception>
    ''' <returns> A Boolean data type. </returns>
    Public Overridable Function TryOpen() As Boolean

        Try
            Me.IsOpen = False
            If System.IO.File.Exists(Me.FilePath) Then
                Me.Open()
                Return Me.IsOpen
            Else
                Return False
            End If

        Catch ex As isr.Configuration.BaseException

            ' close to meet strong guarantees
            Try
                Me.Close()
            Finally
            End Try

            ' throw an exception
            Throw New isr.Configuration.OperationOpenException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                             "Failed opening {0}", Me.FilePath),
                                                                         ex)
        End Try

    End Function

    ''' <summary> Closes the configuration file. </summary>
    ''' <remarks> Use this method to close the instance.  The method returns true if success or false
    ''' if it failed closing the instance. </remarks>
    ''' <returns> A Boolean data type. </returns>
    Public Overridable Function [Close]() As Boolean

        Me.IsOpen = False
        Return Not Me.IsOpen

    End Function

    ''' <summary> Returns a value based on the provided information. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="key">          A <see cref="System.String">String</see> expression that
    ''' specifies the name of the application settings key. </param>
    ''' <param name="defaultValue"> Is an Object expression that specifies the default object to
    ''' return if the key does not exists. </param>
    ''' <returns> Returns an value object of the specified default value. </returns>
    Public Function Fetch(ByVal key As String, ByVal defaultValue As Object) As Object
        If String.IsNullOrWhiteSpace(key) Then
            Throw New ArgumentNullException("key")
        End If
        If defaultValue Is Nothing Then
            Throw New ArgumentNullException("defaultValue")
        End If
        Return Me._fetch(key, defaultValue)
    End Function

    ''' <summary> Returns a value based on the provided information. </summary>
    ''' <remarks> Use this method to add a Key-Value pair. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="key">  A <see cref="System.String">String</see> expression that specifies the
    ''' name of the application settings key. </param>
    ''' <param name="type"> A System.Type expression that specifies the type of the key value to
    ''' return. </param>
    ''' <returns> Returns an value object of the specified type. </returns>
    Public Function Fetch(ByVal key As String, ByVal type As System.Type) As Object
        If String.IsNullOrWhiteSpace(key) Then
            Throw New ArgumentNullException("key")
        End If
        If type Is Nothing Then
            Throw New ArgumentNullException("type")
        End If
        Return Me._fetch(key, type)
    End Function

    ''' <summary> Removes a key from the configuration settings. </summary>
    ''' <remarks> This method uses the <c>Key</c> property. </remarks>
    ''' <returns> A Boolean true if successful. </returns>
    Public Function Remove() As Boolean
        Me._remove(Me._Key)
        Me._Key = Nothing
        Return True
    End Function

    ''' <summary> Removes a key from the configuration settings. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="key"> A <see cref="System.String">String</see> expression that specifies the
    ''' name of the application settings key. </param>
    ''' <returns> A Boolean true if successful. </returns>
    Public Function Remove(ByVal key As String) As Boolean
        If String.IsNullOrWhiteSpace(key) Then
            Throw New ArgumentNullException("key")
        End If
        Me._remove(key)
        If key.Equals(Me._Key) Then
            Me._Key = Nothing
        End If
        Return True
    End Function

    ''' <summary> Saves the application settings to the file. </summary>
    ''' <returns> A Boolean true if success. </returns>
    Public Function Save() As Boolean
        If Me._dirty Then
            Me._xmlDocument.Save(Me.FilePath)
            Me._dirty = False
        End If
        Return True
    End Function

    ''' <summary> Adds a key-value pair to the configuration settings. </summary>
    ''' <remarks> This method uses the <c>Key</c> property. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> Is an Object expression that specifies the key value. </param>
    ''' <returns> A Boolean true if successful. </returns>
    Public Function Update(ByVal value As Object) As Boolean
        If DefaultValue Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        Return Me._update(Me._Key, value)
    End Function

    ''' <summary> Adds a key-value pair to the configuration settings. </summary>
    ''' <remarks> Use this method to add a Key-Value pair. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="key">   A <see cref="System.String">String</see> expression that specifies the
    ''' name of the application settings key. </param>
    ''' <param name="value"> Is an Object expression that specifies the key value. </param>
    ''' <returns> A Boolean true if successful. </returns>
    Public Function Update(ByVal key As String, ByVal value As Object) As Boolean
        If String.IsNullOrWhiteSpace(key) Then
            Throw New ArgumentNullException("key")
        End If
        If DefaultValue Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        Return Me._update(key, value)
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets or sets the file name. </summary>
    ''' <remarks> Use this property to get or set the file name. </remarks>
    ''' <value> <c>FilePath</c> is a String property. </value>
    ''' <example> This example writes a list of Settings to a settings file and then reads the list
    ''' from the file. <code>
    ''' Private Sub form_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '''   '''     Handles MyBase.Click Try
    '''     ' Create an instance of the ISR Scriber Class Dim appSettingsManager As
    '''     isr.Configuration.AppConfig.Scriber ' instantiate a singleton instance of the class
    '''     appSettingsManager = New isr.Configuration.AppConfig.Scriber(fileName)
    '''     appSettingsManager.Open()
    '''       ' add individual values
    '''       .Key = "v5"
    '''       .Value = "Value 5"
    '''       .Key = "v6"
    '''       .Update("Value 6")
    ''' 
    '''       ' remove single values
    '''       .Key = "v4"
    '''       If .KeyExists() Then
    '''         .Remove()
    '''       End If
    ''' 
    '''     End With
    ''' 
    '''     ' add multiple values to a single key With appSettingsManager
    '''       .Key = "MultiValue"
    '''       .Update(New Object() {"Value 7", "Value 8", "Value 9", "Value 10", "Value 11"})
    '''     End With
    ''' 
    '''     ' add a date value With appSettingsManager
    '''       .Key = "DateValue"
    '''       .Type = Type.GetType("system.DateTime")
    '''       .Update(DateTime.Parse("03/20/2003 12:00:00 AM"))
    '''     End With
    ''' 
    '''     ' add an Int32 value With appSettingsManager
    '''     With appSettingsManager
    '''       .Key = "Int32"
    '''       .Update(1001)
    '''     End With
    ''' 
    '''   Catch ex As System.Exception
    '''     ' report failure MessageBox.Show(ex.Message, Me.Name, MessageBoxButtons.OK,
    '''     MessageBoxIcon.Error)
    '''   End Try
    ''' 
    ''' End Sub
    ''' </code>
    ''' To run this example, paste the code fragment into a Windows Form class. Run the program by
    ''' pressing F5, and then click on the form. </example>
    Public Property FilePath() As String

    ''' <summary> Gets the settings default value. </summary>
    ''' <remarks> Use this property to specify the returned value is a setting is new. </remarks>
    ''' <value> <c>DefaultValue</c> is an Object property that specifies the settings default value. </value>
    ''' <seealso cref="AppConfigScriber.FilePath"/>
    Public Property DefaultValue() As Object

    Private _element As System.Xml.XmlElement

    ''' <summary> Returns the application settings XML element. </summary>
    ''' <value> <c>Element</c> is an XmlElement property. </value>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1059:MembersShouldNotExposeCertainConcreteTypes", MessageId:="System.Xml.XmlNode")>
    Public ReadOnly Property Element() As System.Xml.XmlElement
        Get
            Return Me._element
        End Get
    End Property

    Private _dirty As Boolean

    ''' <summary> Is true if the configuration settings file has changes to be saved. </summary>
    ''' <value> <c>HasPendingChanges</c> is a Boolean property. </value>
    Public ReadOnly Property HasPendingChanges() As Boolean
        Get
            Return Me._dirty
        End Get
    End Property

    ''' <summary> Is true if the application setting changes since the application started. </summary>
    ''' <value> <c>SettingChanged</c> is a Boolean property. </value>
    Public ReadOnly Property SettingChanged() As Boolean
        Get
            Return Me._settingChanged(Me._Key)
        End Get
    End Property

    ''' <summary> Is true if the application setting key exists. </summary>
    ''' <value> <c>KeyExists</c> is a Boolean property. </value>
    Public ReadOnly Property KeyExists() As Boolean
        Get
            Return Me._keyExists(Me._Key)
        End Get
    End Property

    ''' <summary> Is true if the application setting key value exists. </summary>
    ''' <value> <c>KeyExists</c> is a Boolean property. </value>
    Public ReadOnly Property ValueExists(ByVal keyValue As Object) As Boolean
        Get
            Return Me._valueExists(Me._Key, keyValue)
        End Get
    End Property

    ''' <summary> Gets or sets the Format of the setting. It determines the string format used to store
    ''' the settings for single, double, date, or currency values. </summary>
    ''' <remarks> Use this property to determine how numeric values are to be stored in the settings
    ''' file. The format string conforms to the specification of the Visual Basic Format function. </remarks>
    ''' <value> <c>settingFormat</c> is a String property that can be read from or written to (read
    ''' or write) that specifies the format of the string that is used to store a numeric value in
    ''' the settings file. </value>
    ''' <seealso cref="AppConfigScriber.FilePath"/>
    Public Property Format() As String

    ''' <summary> Gets or sets the key name of the setting. </summary>
    ''' <remarks> Use this property to set or get the name of the settings string. </remarks>
    ''' <value> <c>Key</c> is a String property that can be read from or written to (read or write)
    ''' that specifies the name of the settings string in the settings file. </value>
    ''' <seealso cref="AppConfigScriber.FilePath"/>
    Public Property Key() As String

    Private _value As Object

    ''' <summary> Gets or sets the settings value. </summary>
    ''' <remarks> Use this property to read or write a settings value. The setting value is read from
    ''' the settings file specified in the <see cref="FilePath">File Path</see> property. The value
    ''' returned depends on the variable type of the
    ''' <see cref="DefaultValue">Default Value</see> property.  If the
    ''' <see cref="DefaultValue">Default Value</see> property is Empty or Null, the type is set to
    ''' String. The value that is written to the settings file is determined by the settingFormat
    ''' property. </remarks>
    ''' <value> <c>Value</c> is an Object property that can be read from or written to (read or
    ''' write) that specifies the settings value. </value>
    ''' <seealso cref="AppConfigScriber.FilePath"/>
    Public Property Value() As Object
        Get
            ' Read the setting.
            If DefaultValue Is Nothing Then
                Me._value = Me._fetch(Me.Key, Me.SystemType)
            Else
                Me._value = Me._fetch(Me.Key, Me.DefaultValue)
            End If
            ' return the value that were read
            Return Me._value
        End Get
        Set(ByVal value As Object)
            Me._value = value
            Me._update(Me.Key, value)
        End Set
    End Property

    ''' <summary> Gets the settings type. </summary>
    ''' <remarks> Use this property to define the settings type. </remarks>
    ''' <value> <c>Type</c> is a Type property that can be read from or written to (read or write)
    ''' that specifies the settings type. </value>
    ''' <seealso cref="AppConfigScriber.FilePath"/>
    ''' <history date="09/09/03" by="David" revision="1.0.1341.x"> Rename to SystemType to
    ''' comply with Microsoft Naming rule CA1721:PropertyNamesShouldNotMatchGetMethods. </history>
    Public Property SystemType() As System.Type

    Private _xmlDocument As System.Xml.XmlDocument

    ''' <summary> Gets the Application Configuration setting document. </summary>
    ''' <remarks> Use this property to gain direct access to the application setting document. </remarks>
    ''' <value> <c>XmlDocument</c> is a read only property. </value>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1059:MembersShouldNotExposeCertainConcreteTypes", MessageId:="System.Xml.XmlNode")>
    Public ReadOnly Property XmlDocument() As System.Xml.XmlDocument
        Get
            Return Me._xmlDocument
        End Get
    End Property

#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary> Creates a new application settings element. </summary>
    ''' <param name="keyName"> A <see cref="System.String">String</see> expression that specifies
    ''' the name of the application settings key. </param>
    ''' <returns> Returns the application setting element. </returns>
    Private Function _createElement(ByVal keyName As String) As System.Xml.XmlElement
        Dim el As System.Xml.XmlElement = Me._xmlDocument.CreateElement(attributeName)
        el.SetAttribute(elementKeyName, keyName)
        el.SetAttribute(elementValueName, "")
        Me._element.AppendChild(el)
        Me._dirty = True
        Return el
    End Function

    ''' <summary> Creates a new application settings element. </summary>
    ''' <param name="keyName"> A <see cref="System.String">String</see> expression that specifies
    ''' the name of the application settings key. </param>
    ''' <param name="value">   Is an Object expression that specifies the value to add. </param>
    ''' <returns> Returns the application setting element. </returns>
    Private Function _createElement(ByVal keyName As String, ByVal value As String) As System.Xml.XmlElement
        Dim el As System.Xml.XmlElement = Me._createElement(keyName)
        el.SetAttribute(elementValueName, value)
        Me._dirty = True
        Return el
    End Function

    ''' <summary> Gets an application settings string. </summary>
    ''' <param name="keyName"> A <see cref="System.String">String</see> expression that specifies
    ''' the name of the application settings key. </param>
    ''' <returns> A <see cref="System.String">String</see> data type holding the application settings. </returns>
    Private Function _fetch(ByVal keyName As String) As String

        Dim keyElement As System.Xml.XmlElement = Me._keyElement(keyName)
        If keyElement Is Nothing Then
            Return Nothing
        Else
            Return keyElement.GetAttribute(elementValueName)
        End If

    End Function

    ''' <summary> Gets an application settings. </summary>
    ''' <remarks> If the key is new, the key is created with the default value. </remarks>
    ''' <param name="keyName">      A <see cref="System.String">String</see> expression that
    ''' specifies the name of the application settings key. </param>
    ''' <param name="defaultValue"> Is an Object expression that specifies the default value to
    ''' return if the key is new. </param>
    ''' <returns> Returns an Object data of the same type as the default value. </returns>
    ''' <history date="04/18/05" by="David" revision="1.0.1934.x"> Use
    ''' isr.Configuration.Conversions. </history>
    Private Function _fetch(ByVal keyName As String, ByVal defaultValue As Object) As Object

        ' check if the key exists 
        If Not Me._keyExists(keyName) Then
            Me._update(keyName, defaultValue)
        End If
        Return isr.Configuration.Conversions.Deserialize(Me._fetch(keyName), defaultValue)

    End Function

    ''' <summary> Gets an application settings. </summary>
    ''' <param name="keyName"> A <see cref="System.String">String</see> expression that specifies
    ''' the name of the application settings key. </param>
    ''' <param name="keyType"> A System.Type expression that specifies the type of the application
    ''' settings to get. </param>
    ''' <returns> Returns an Object data of the specified type. </returns>
    ''' <history date="04/18/05" by="David" revision="1.0.1934.x"> Use
    ''' isr.Configuration.Conversions. </history>
    Private Function _fetch(ByVal keyName As String, ByVal keyType As System.Type) As Object

        Return isr.Configuration.Conversions.Deserialize(Me._fetch(keyName), keyType)

    End Function

    ''' <summary> Checks if a setting changed since the application loaded. </summary>
    ''' <param name="keyname"> A <see cref="System.String">String</see> expression that specifies
    ''' the name of the application settings key. </param>
    ''' <returns> Returns the application setting element. </returns>
    Private Function _settingChanged(ByVal keyname As String) As Boolean

        ' get the loaded setting value
        Dim loadedValue As String = CType(System.Configuration.ConfigurationManager.AppSettings.Get(keyname), String)
        Dim keyValue As String = Me._fetch(keyname)
        If loadedValue Is Nothing Then
            Return keyValue IsNot Nothing
        Else
            Return Not loadedValue.Equals(keyValue)
        End If

    End Function

    ''' <summary> Checks if a key exists. </summary>
    ''' <param name="keyName"> A <see cref="System.String">String</see> expression that specifies
    ''' the name of the application settings key. </param>
    ''' <returns> Returns true if the key exists. </returns>
    Private Function _keyExists(ByVal keyName As String) As Boolean
        Return Me._keyElement(keyName) IsNot Nothing
    End Function

    ''' <summary> Checks if a key value exists. </summary>
    ''' <param name="keyName"> A <see cref="System.String">String</see> expression that specifies
    ''' the name of the application settings key. </param>
    ''' <param name="value">   Is an Object expression that specifies the value. </param>
    ''' <returns> Returns true if the value exists. </returns>
    ''' <history date="04/18/05" by="David" revision="1.0.1934.x"> Use
    ''' isr.Configuration.Conversions. </history>
    Private Function _valueExists(ByVal keyName As String, ByVal value As Object) As Boolean
        Dim convertedValue As String = isr.Configuration.Conversions.Serialize(value)
        Dim existingValue As String = Me._fetch(keyName)
        Return convertedValue.Equals(existingValue)
    End Function

    ''' <summary> Gets the settings key XML element. </summary>
    ''' <param name="keyName"> A <see cref="System.String">String</see> expression that specifies
    ''' the name of the application settings key. </param>
    ''' <returns> Returns an XmlElement Object including the key data. </returns>
    Private Function _keyElement(ByVal keyName As String) As System.Xml.XmlElement
        Return CType(Element.SelectSingleNode(String.Format(Globalization.CultureInfo.CurrentCulture, elementKeyXpathFormat, keyName)), 
            System.Xml.XmlElement)
    End Function

    ''' <summary> Removes the key from the application settings. </summary>
    ''' <param name="keyName"> A <see cref="System.String">String</see> expression that specifies
    ''' the name of the application settings key. </param>
    ''' <returns> Returns true if successful.. </returns>
    Private Function _remove(ByVal keyName As String) As Boolean
        Dim el As System.Xml.XmlElement = Me._keyElement(keyName)
        If el IsNot Nothing Then
            el.ParentNode.RemoveChild(el)
            Me._dirty = True
        End If
        Return True
    End Function

    ''' <summary> updates an existing key values or adds a new key-value pair. </summary>
    ''' <param name="keyName"> A <see cref="System.String">String</see> expression that specifies
    ''' the name of the application settings key. </param>
    ''' <param name="value">   Is an Object expression that specifies the value to add. </param>
    ''' <returns> Returns true if successful. </returns>
    ''' <history date="04/18/05" by="David" revision="1.0.1934.x"> Use
    ''' isr.Configuration.Conversions. </history>
    Private Function _update(ByVal keyName As String, ByVal value As Object) As Boolean

        '    Dim keyValue As String = CType(value, System.String) ' pre 1.0.1348
        Dim keyValue As String = isr.Configuration.Conversions.Serialize(value)
        ' check if have This key
        If Me._keyExists(keyName) Then
            ' if so, remove the key first
            Me._remove(keyName)
            Me._dirty = True
        End If
        ' Date.Now create the key value pair
        Me._createElement(keyName, keyValue)
        Return True
    End Function

#End Region

End Class
