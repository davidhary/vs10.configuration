''' <summary> Provides services for saving user application preferences. </summary>
''' <remarks> Use this class to save user-specific application preferences. </remarks>
''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/09/03" by="David" revision="1.0.1347.x"> Created. </history>
Public Class UserPreferencesScriber

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary> Initializes a new instance of the <see cref="UserPreferencesScriber" /> class. </summary>
    Public Sub New()
        Me.New(System.Windows.Forms.Application.ExecutablePath & ProfileScriber.DefaultExtension)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="UserPreferencesScriber" /> class. </summary>
    ''' <param name="filePath"> The file path. This also serves as the instance name. </param>
    Public Sub New(ByVal filePath As String)
        MyBase.New()
        Me._FilePath = filePath
        Me._DefaultFileName = System.Windows.Forms.Application.ExecutablePath & UserPreferencesScriber.DefaultExtension
    End Sub

#End Region

#Region " BASE METHODS AND PROPERTIES "

    ''' <summary> Gets or sets the full name of the file. </summary>
    ''' <value> The name of the file. </value>
    Public Property FilePath() As String

    ''' <summary> Validates the name of the file. </summary>
    ''' <param name="filePath"> The file path. </param>
    ''' <returns> <c>True</c> if file name is valid, <c>False</c> otherwise. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Shared Function ValidateFileName(ByVal filePath As String) As Boolean
        Try
            ' Check that the file name is legal.
            System.IO.File.Exists(filePath)
            Return True
        Catch
            Return False
        End Try
    End Function

    ''' <summary> Gets or sets the default name of the file. </summary>
    ''' <value> The default name of the file. </value>
    Public Property DefaultFileName As String

#End Region


#Region " SHARED "

    ''' <summary> Holds the default extension of a configuration file. </summary>
    Public Const DefaultExtension As String = ".config"

#End Region

#Region " PROTECTED MEMBERS "

    ''' <summary> The encoding page. 
    '''           Code page 1252 preserves ASCII through 255. </summary>
    Private Shared _encodingPage As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)
    '  Private Shared _encodingPage As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)
    Protected Shared ReadOnly Property EncodingPage() As System.Text.Encoding
        Get
            Return UserPreferencesScriber._encodingPage
        End Get
    End Property

#End Region

#Region " NESTED CLASSES/STRUCTURES "

    ''' <summary>This structure is used to de/serialize the information to a stream 
    ''' (in this case, a binary file).</summary>
    <Serializable()> Private Structure PersistData

        ''' <summary> Gets the key. </summary>
        ''' <value> The key. </value>
        Public Property Key() As String

        ''' <summary> Gets the value. </summary>
        ''' <value> The value. </value>
        Public Property Value() As String
    End Structure

    ''' <summary> Allows changes to its values. This is how the data is 
    '''           cached/manipulated at runtime.</summary>
    ''' <history date="09/09/03" by="David" revision="1.0.1347.x">  Created </history>  
    Private Class SettingData
        Public Sub New(ByVal data As PersistData)
            MyBase.new()
            Me._Key = data.Key
            Me._value = data.Value
            Me._Persist = True
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <param name="keyName"> Name of the key. </param>
        ''' <param name="value">   The value. </param>
        Public Sub New(ByVal keyName As String, ByVal value As String)
            MyBase.new()
            Me.Key = keyName
            Me._value = value
            Me._changed = True
            Me.Persist = False
        End Sub
#If False Then
            Public Property ID() As Int32
            Public Property Group() As String
#End If
        Public Property Key() As String
        Public Property Persist() As Boolean
        Private _changed As Boolean

        Public ReadOnly Property Changed() As Boolean
            Get
                Return Me._changed
            End Get
        End Property

        Private _value As String
        Public Property Value() As String
            Get
                Return Me._value
            End Get
            Set(ByVal value As String)
                If Me._value <> value Then
                    Me._changed = True
                    Me._value = value
                End If
            End Set
        End Property

        ''' <summary> Gets the data. </summary>
        ''' <returns> The data. </returns>
        Public Function GetData() As PersistData
            Dim pd As New PersistData
            pd.Key = Me.Key
            pd.Value = Me.Value
            Return pd
        End Function

        ''' <summary> Serves as a hash function for a particular type. </summary>
        ''' <returns> A hash code for the current <see cref="T:System.Object" />. </returns>
        Public Overloads Overrides Function GetHashCode() As Int32
            Return HashCode(Me._Key)
        End Function

        ''' <summary> Hash code. </summary>
        ''' <param name="keyName"> Name of the key. </param>
        ''' <returns> A hash code for the current <see cref="T:System.Object" />. </returns>
        Public Shared Function HashCode(ByVal keyName As String) As Int32
            ' use the keyName for lookups
            Return keyName.ToUpper(Globalization.CultureInfo.CurrentCulture).GetHashCode
        End Function
    End Class

#End Region

#Region " ATOMIC OPERATIONS "

    ' These functions would support "atomic" persistence changes
    ' (record-based create, update, delete, such as in a database).
    ' File-based persistence is a "batch" persistence mechanism,
    ' meaning ALL data (for a user) must be saved at once.

    ' Add your code to delete from database, registry, etc.

    ''' <summary> Deletes the data item described by Setting. </summary>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    ''' unimplemented. </exception>
    ''' <param name="Setting"> The setting. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="Setting")>
    Private Shared Sub DeleteDataItem(ByVal Setting As SettingData)
        Throw New NotImplementedException("Delete Data Item has not been implemented.")
    End Sub

    ' Add your code to save to database, registry, etc.

    ''' <summary> Saves a new data item. </summary>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    ''' unimplemented. </exception>
    ''' <param name="Setting"> The setting. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="Setting")>
    Private Shared Sub SaveNewDataItem(ByVal Setting As SettingData)
        If Setting.Changed Then
            Throw New NotImplementedException("Save New Data Item has not been implemented.")
        End If
    End Sub

    ' Add your code to update database, registry, etc.

    ''' <summary> Updates the data item described by Setting. </summary>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    ''' unimplemented. </exception>
    ''' <param name="Setting"> The setting. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId:="Setting")>
    Private Shared Sub UpdateDataItem(ByVal Setting As SettingData)
        If Setting.Changed Then
            Throw New NotImplementedException("Update Data Item has not been implemented.")
        End If
    End Sub

#End Region

#Region " METHODS "

    ''' <summary> Fetches the user preferences from the file. </summary>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Public Function ReadAll() As Boolean
        Me._preferences = Me.readUserPreferences(Me.FilePath)
        Return True
    End Function

    ''' <summary> Saves the application settings to the file. </summary>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Public Function WriteAll() As Boolean
        If Me._dirty Then
            Me.writeUserPreferences(Me.FilePath, Me._preferences)
            Me._dirty = False
        End If
        Return True
    End Function

    ''' <summary> Fetches the specified key name. </summary>
    ''' <param name="keyName">      Name of the key. </param>
    ''' <param name="defaultValue"> The default value. </param>
    ''' <returns> System.Object. </returns>
    Public Function Fetch(ByVal keyName As String, ByVal defaultValue As Object) As Object
        Me.Key = keyName
        Me.DefaultValue = defaultValue
        Return Me.Value
    End Function

    ''' <summary> Updates this object. </summary>
    ''' <param name="keyName"> Name of the key. </param>
    ''' <param name="value">   Is the object preference to save. </param>
    ''' <param name="persist"> A Boolean expression to set true for persisting the preference. If
    ''' Persist = True and AutoSave is enabled, the value will also be inserted/updated in the
    ''' persistence store. </param>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Public Function Update(ByVal keyName As String, ByVal value As Object, ByVal persist As Boolean) As Boolean

        Me.Key = keyName
        Me.Persist = persist
        Me.Value = value
        Return True

    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Determines how preferences are saved. </summary>
    ''' <value> <c>AutoSave</c>is a <see cref="Boolean"/> property where True means new or changed
    ''' preferences (Persist=True) are saved immediately (such as saving to a database or registry,
    ''' where atomic updates are possible); False means you must explicitly call SavePreferences
    ''' (such as with file storage, where atomic updates are not (easily) done.) </value>
    ''' <history date="09/09/03" by="David" revision="1.0.1347.x"> Create. </history>
    Public Property AutoSave() As Boolean

    Private _preferences As Hashtable

    ''' <summary> Gets or sets the settings default value. </summary>
    ''' <remarks> Use this property to set or get the default value for the setting.. </remarks>
    ''' <value> <c>DefaultValue</c> is a String property that can be read from or written to (read or
    ''' write) that specifies the settings default value. </value>
    Public Property DefaultValue() As Object

    Private _dirty As Boolean

    ''' <summary> Is true if the configuration settings file has changes to be saved. </summary>
    ''' <value> <c>HasPendingChanges</c> is a Boolean property. </value>
    Public ReadOnly Property HasPendingChanges() As Boolean
        Get
            Return Me._dirty
        End Get
    End Property

    ''' <summary> Gets the key exists. </summary>
    ''' <value> The key exists. </value>
    Public ReadOnly Property KeyExists() As Boolean
        Get
            Dim keyHash As Int32 = SettingData.HashCode(Me._Key)
            Return Me._preferences.ContainsKey(keyHash)
        End Get
    End Property

    ''' <summary> Returns true if the key value exists. </summary>
    ''' <value> The value exists. </value>
    ''' <param name="keyValue"> Specifies the key value to check. </param>
    ''' <history date="04/18/05" by="David" revision="1.0.1934.x"> Use
    ''' isr.Configuration.Conversions. </history>
    Public ReadOnly Property ValueExists(ByVal keyValue As Object) As Boolean
        Get
            Dim newValue As String
            Dim data As SettingData
            Dim keyHash As Int32 = SettingData.HashCode(Me._Key)
            If Me._preferences.ContainsKey(keyHash) Then
                newValue = isr.Configuration.Conversions.Serialize(keyValue)
                data = CType(Me._preferences.Item(keyHash), SettingData)
                Return (data.Value = newValue)
            Else
                Return False
            End If
        End Get
    End Property

    ''' <summary> Gets or sets the key name of the preference setting. </summary>
    ''' <remarks> Use this property to set or get the name of the settings string. </remarks>
    ''' <value> <c>Key</c> is a String property that can be read from or written to (read or write)
    ''' that specifies the name of the settings string in the settings file. </value>
    Public Property Key() As String

    ''' <summary> Gets or sets a value indicating whether this <see cref="UserPreferencesScriber" /> is persist. </summary>
    ''' <value> <c>True</c> if persist; otherwise, <c>False</c>. </value>
    Public Property Persist() As Boolean

    ''' <summary> Is true if the configuration settings formatter uses SOAP. </summary>
    ''' <value> <c>UseSoap</c> is a Boolean property. </value>
    Public Property UseSoap() As Boolean

    ''' <summary> Gets or sets the settings value. </summary>
    ''' <remarks> Use this property to read or write a settings value. The setting value is read from
    ''' the settings preference file specified in the <see cref="FilePath">File Path</see> property.  If the <see cref="FilePath">File Path</see> property is
    ''' Empty, the value is read from the application data folder in the use documents and settings
    ''' folder. </remarks>
    ''' <value> <c>Value</c> is a String property that can be read from or written to (read or write)
    ''' that specifies the settings value. </value>
    Public Property Value() As Object
        Get
            ' Read the setting.
            Return UserPreferencesScriber.fetchUserPreference(Me._Key, Me.DefaultValue, Me._preferences)
        End Get
        Set(ByVal value As Object)
            Me.updateUserPreference(Me._Key, value, Me.Persist, Me.AutoSave, Me._preferences)
        End Set
    End Property

#End Region

#Region " ON EVENT HANDLERS "

    ''' <summary> Gets a preference from the cache. </summary>
    ''' <remarks> GetPreference: returns defaultValue if not found, or on certain errors. USE THE SAME
    ''' TYPE for SavePreference() AND defaultValue!  
    ''' The type of the defaultValue determines HOW the value is restored. </remarks>
    ''' <param name="keyName">      A <see cref="System.String">String</see> expression that
    ''' specifies the preference key name. </param>
    ''' <param name="defaultValue"> Is an Object expression that specifies the default value. </param>
    ''' <param name="preferences">  A Hash table holding the exiting preferences. </param>
    ''' <returns> Returns an Object preference. </returns>
    ''' <history date="04/18/05" by="David" revision="1.0.1934.x"> Use isr.Configuration.Conversions. </history>
    Private Shared Function fetchUserPreference(ByVal keyName As String, ByVal defaultValue As Object, ByVal preferences As Hashtable) As Object

        If preferences.ContainsKey(SettingData.HashCode(keyName)) Then

            ' if the key exists, get the value
            Dim data As SettingData
            data = CType(preferences.Item(SettingData.HashCode(keyName)), SettingData)

            Return isr.Configuration.Conversions.Deserialize(data.Value, defaultValue)

        Else ' not saved, return default 

            Return defaultValue

        End If

    End Function

    ''' <summary> updates or adds keyName/value to internal cache. Use Value = Nothing to delete a
    ''' preference. </summary>
    ''' <param name="keyName">     A <see cref="System.String">String</see> expression that
    ''' specifies the preference keyName name. </param>
    ''' <param name="value">       Is the object preference to save. </param>
    ''' <param name="persist">     A Boolean expression to set true for persisting the preference.
    ''' If Persist = True and AutoSave is enabled, the value will also be inserted/updated in the
    ''' persistence store. </param>
    ''' <param name="autoSave">    A Boolean expression to set true to auto save. </param>
    ''' <param name="preferences"> A Hash table holding the exiting preferences. </param>
    ''' <history date="04/18/05" by="David" revision="1.0.1934.x"> Use isr.Configuration.Conversions. </history>
    Private Sub updateUserPreference(ByVal keyName As String, ByVal value As Object, ByVal persist As Boolean,
                                     ByVal autoSave As Boolean, ByVal preferences As Hashtable)

        Dim newValue As String
        Dim keyHash As Int32 = SettingData.HashCode(keyName)
        Dim data As SettingData
        If value Is Nothing Then ' This is a DELETE
            ' remove from local cache
            If preferences.ContainsKey(keyHash) Then
                data = CType(preferences.Item(keyHash), SettingData)
                preferences.Remove(keyHash)
                If persist AndAlso autoSave Then
                    ' delete from persistence store
                    DeleteDataItem(data)
                End If
            End If
            Exit Sub

        Else

            newValue = isr.Configuration.Conversions.Serialize(value)

            ' Update cache with current value
            If preferences.ContainsKey(keyHash) Then
                data = CType(preferences.Item(keyHash), SettingData)
                If data.Value <> newValue Then
                    data.Value = newValue
                    Me._dirty = True
                    If persist AndAlso autoSave Then
                        UpdateDataItem(data)
                    End If
                End If
            Else
                data = New SettingData(keyName, newValue)
                data.Persist = persist
                preferences.Add(keyHash, data)
                Me._dirty = True
                If persist AndAlso autoSave Then
                    SaveNewDataItem(data)
                End If
            End If
        End If

    End Sub

    ''' <summary> Gets a default file name. </summary>
    ''' <remarks> The file name defaults to the Application Data folder of the application x:\Documents
    ''' and Settings\UserName\Application Data\'executable file title'\Preferences.dat. </remarks>
    ''' <returns> A <see cref="System.String">String</see> data type. </returns>
    Private Shared Function defaultFilePath() As String
        Return IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                               "\" & Windows.Forms.Application.ProductName & "\Preferences.dat")
    End Function

    ''' <summary> Read preferences from file. </summary>
    ''' <remarks> Use This method to read preferences from the preference file. If filePath not
    ''' provided, the default filePath is used. </remarks>
    ''' <exception cref="BaseException"> Thrown when a Base error condition occurs. </exception>
    ''' <param name="filePath"> A <see cref="System.String">String</see> expression that specifies
    ''' the file name.  
    ''' Use an empty file name if you wish to use the default file name. </param>
    ''' <returns> The user preferences. </returns>
    Private Function readUserPreferences(ByVal filePath As String) As Hashtable

        ' load from user file...
        If filePath.Length < 4 Then
            filePath = defaultFilePath()
        End If
        Dim preferences As Hashtable = New Hashtable
        If System.IO.File.Exists(filePath) Then
            Dim persist As PersistData()
            Try
                Using stream As IO.FileStream = System.IO.File.OpenRead(filePath)
                    If Me.UseSoap Then
                        Dim formatter As New System.Runtime.Serialization.Formatters.Soap.SoapFormatter
                        Try
                            ' deserialize the whole she-bang
                            persist = CType(formatter.Deserialize(stream), PersistData())
                        Catch ex As System.Runtime.Serialization.SerializationException
                            Throw New BaseException("Unable to deserialize: " & ex.Message, ex)
                        End Try
                    Else
                        Dim formatter As New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
                        Try
                            ' deserialize the whole she-bang
                            persist = CType(formatter.Deserialize(stream), PersistData())
                        Catch ex As System.Runtime.Serialization.SerializationException
                            Throw New BaseException("Unable to deserialize: " & ex.Message, ex)
                        End Try
                    End If
                    Dim i As Int32, setting As SettingData
                    ' Date.Now loop through, creating a SettingData object
                    ' for each keyName/value in the array.
                    For i = 0 To persist.Length - 1
                        setting = New SettingData(persist(i))
                        preferences.Add(setting.GetHashCode, setting)
                    Next
                End Using
            Catch ex As System.IO.IOException
                Throw New BaseException("Unable to read from file: " & ex.Message, ex)
            End Try
        End If
        Return preferences
        Me._dirty = False
    End Function

    ''' <summary> Writes a user preferences.
    '''           Save preferences to file.  If filePath not provided, the default filePath is used. </summary>
    ''' <exception cref="BaseException"> Thrown when a Base error condition occurs. </exception>
    ''' <param name="filePath">    Name of the file path. This also serves as the instance name. </param>
    ''' <param name="preferences"> A Hash table holding the exiting preferences. </param>
    Private Sub writeUserPreferences(ByVal filePath As String, ByVal preferences As Hashtable)

        If preferences IsNot Nothing Then

            ' save to user file...
            If filePath.Length < 4 Then
                filePath = defaultFilePath()
            End If

            Dim de As DictionaryEntry, setting As SettingData
            Dim persist As PersistData(), pindex As Int32 = 0
            ' Create array to hold keyName/values:
            ' initialize capacity to total items
            ReDim persist(preferences.Count)

            ' get all persistable settings from cache:
            For Each de In preferences
                setting = CType(de.Value, SettingData)
                If setting.Persist Then ' if saving to db, you would
                    ' also want to look at Dirty.
                    ' add data to array
                    persist(pindex) = setting.GetData()
                    pindex += 1
                End If
            Next

            ' remove existing file
            If System.IO.File.Exists(filePath) Then
                Try
                    System.IO.File.Delete(filePath)
                Catch ex As System.IO.IOException
                    Throw New BaseException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                          "Unable to remove file: {0}{1}{2}",
                                                          filePath, Environment.NewLine, ex.Message), ex)
                    Exit Sub
                End Try
            End If

            If pindex > 0 Then

                ' shrink array to persistable count:
                ReDim Preserve persist(pindex - 1)

                ' Open target file:
                Dim fi As New System.IO.FileInfo(filePath)
                ' ensure path exists:
                If Not System.IO.Directory.Exists(fi.Directory.FullName) Then
                    System.IO.Directory.CreateDirectory(fi.Directory.FullName)
                End If
                Using stream As IO.FileStream = System.IO.File.Create(filePath)
                    If Me.UseSoap Then
                        Dim formatter As New System.Runtime.Serialization.Formatters.Soap.SoapFormatter
                        ' serialize whole array to file:
                        formatter.Serialize(stream, persist)
                    Else
                        Dim formatter As New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
                        ' serialize whole array to file:
                        formatter.Serialize(stream, persist)
                    End If
                    Me._dirty = False
                End Using
            End If
        End If

    End Sub

#End Region

End Class
