Imports System.ComponentModel
''' <summary> Provides a base class for forms that have automatic preferences. </summary>
''' <remarks> Inherit this class in forms where you wish to have form preferences. It contains
''' several new Overridable methods to fetch user preferences, perform data loads, etc. AFTER the
''' form is visible or save user preferences. It also has a public property: SaveSettings
''' (Boolean) which to set to True (default)
''' to allow the window's size and position to be automatically saved and restored NOTE that the
''' window uses the Form Name to store its data, so multiple instances with the same name (e.g.
''' the same class) will use the same settings. </remarks>
''' <license> (c) 2003 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/09/03" by="David" revision="1.0.1347.x"> Created. </history>
Public Class UserPreferencesForm
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

        ' instantiate an instance of the user preferences class
        Me._userPreferences = New isr.Configuration.UserPreferencesScriber

    End Sub

    ''' <summary> Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />. </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    ''' release only unmanaged resources. </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If disposing Then

                If components IsNot Nothing Then
                    components.Dispose()
                End If

            End If

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try

    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(UserPreferencesForm))
        '
        'ScriberForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 273)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "ScriberForm"
        Me.Text = "Scriber Form"
    End Sub

#End Region

#Region " TYPES "

    Private Shared ReadOnly suffix As String = "_key"

    ''' <summary>This structure holds the default form settings to save and restore.</summary>
    <Serializable()> Private Structure WindowSettings
        Public Sub New(ByVal form As System.Windows.Forms.Form)
            Me._X = form.Location.X
            Me._Y = form.Location.Y
            Me._Size = form.Size
            Me._State = form.WindowState
        End Sub

        ''' <summary> Gets the size. </summary>
        ''' <value> The size. </value>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Property Size() As System.Drawing.Size

        ''' <summary> Gets the X. </summary>
        ''' <value> The X. </value>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Property X() As Int32

        ''' <summary> Gets the Y. </summary>
        ''' <value> The Y. </value>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Public Property Y() As Int32

        ''' <summary> Gets the state. </summary>
        ''' <value> The state. </value>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        <DesignerSerializationVisibility(System.ComponentModel.DesignerSerializationVisibility.Hidden), Browsable(False)>
        Public Property State() As System.Windows.Forms.FormWindowState

    End Structure

#End Region

#Region " Protected Overridable Methods "

    ''' <summary> Is called when the form has loaded and is visible to the user. </summary>
    ''' <remarks> Use this method fetch data, enable/disable controls, etc. </remarks>
    <PermissionSetAttribute(SecurityAction.Demand, Unrestricted:=True)>
    Protected Overridable Sub OnRendered()
    End Sub

    ''' <summary> This is called when the form is loaded before it is visible. </summary>
    ''' <remarks> Use this method fetch settings. </remarks>
    <PermissionSetAttribute(SecurityAction.Demand, Unrestricted:=True)>
    Protected Overridable Sub OnLoadSettings()

        If Me._SaveSettings Then
            Me._userPreferences.ReadAll()
            Dim prior As New WindowSettings(Me)
            With Me._userPreferences
                .Key = Me.Name & suffix
                .DefaultValue = prior
                prior = CType(.Value, WindowSettings)
            End With
            If prior.X >= Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width OrElse
                prior.Y >= Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height Then
                ' window would appear off-screen!
            Else
                If prior.State = Windows.Forms.FormWindowState.Normal Then
                    Me.Size = prior.Size
                    Me.Left = prior.X
                    Me.Top = prior.Y
                Else
                    Me.WindowState = prior.State
                End If
            End If
        End If
    End Sub

    ''' <summary> Is called when the form unloads. </summary>
    ''' <remarks> Use Save settings. </remarks>
    <PermissionSetAttribute(SecurityAction.Demand, Unrestricted:=True)>
    Protected Overridable Sub OnSaveSettings()
        If Me._SaveSettings Then
            With Me._userPreferences
                .Key = Me.Name & suffix
                .Persist = True
                .Value = New WindowSettings(Me)
            End With
        End If
    End Sub

    ''' <summary> Is called when the form is loading (not yet visible). </summary>
    ''' <remarks> Use this method fetch settings when in run time. </remarks>
    <PermissionSetAttribute(SecurityAction.Demand, Unrestricted:=True)>
    Protected Overrides Sub OnCreateControl()
        MyBase.OnCreateControl()
        Me._loaded = False
        If Not Me.DesignMode Then
            Me.OnLoadSettings()
        End If
    End Sub

    ''' <summary> Handles the FIRST activation (got focus) for this instance. </summary>
    Protected Sub Activator()
        If Not Me._loaded Then
            Me._loaded = True ' Don't reenter block!!
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            System.Windows.Forms.Application.DoEvents() ' allow window to display
            Me.Refresh()
            OnRendered() ' call to OnRendered
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End If
    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Occurs when the form gets focus. </summary>
    ''' <remarks> Use this event to call the Activator method. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnActivated(ByVal e As System.EventArgs)
        MyBase.OnActivated(e)
        Me.Activator()
    End Sub

    ''' <summary> Occurs when the form is closing. </summary>
    ''' <remarks> Use this event save window position only at Run-Time!! </remarks>
    ''' <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
    ''' event data. </param>
    Protected Overrides Sub OnClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        If Not DesignMode Then
            Me.OnSaveSettings()
        End If
        MyBase.OnClosing(e)
    End Sub

#End Region

#Region " Protected Properties "

    Private _userPreferences As isr.Configuration.UserPreferencesScriber

    ''' <summary> Gets the scriber. </summary>
    ''' <value> The scriber. </value>
    Protected ReadOnly Property Scriber() As isr.Configuration.UserPreferencesScriber
        Get
            Return Me._userPreferences
        End Get
    End Property

#End Region

#Region " Public Methods "

    ''' <summary> Gets or sets the flag allowing to active the form only once. </summary>
    ''' <value> <c>loaded</c>is a Boolean property. </value>
    Private Property loaded() As Boolean

    ''' <summary> Controls if user preferences are saved. </summary>
    ''' <remarks> Set this property to false to disable size/position saves. </remarks>
    ''' <value> <c>SaveSettings</c>is a Boolean property. </value>
    Public Property SaveSettings() As Boolean

#End Region

End Class
