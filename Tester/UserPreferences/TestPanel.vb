Namespace UserPreferences

    ''' <summary> Includes code for form TestPanel. </summary>
    ''' <remarks> Launch this form by calling its Show or ShowDialog method from its default instance. </remarks>
    ''' <license> (c) 2003 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="09/09/03" by="David" revision="1.0.1347.x"> Created. </history>
    Friend Class TestPanel
        Inherits isr.Configuration.UserPreferencesForm

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()

            'This call is required by the Windows Form Designer.
            InitializeComponent()

            'Add any initialization after the InitializeComponent() call

        End Sub

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            Try

                If disposing Then

                    If components IsNot Nothing Then
                        components.Dispose()
                    End If

                End If

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Private WithEvents freeFormTextBox As System.Windows.Forms.TextBox
        Private WithEvents fontButton As System.Windows.Forms.Button
        Private WithEvents GroupBox1 As System.Windows.Forms.GroupBox
        Private WithEvents CheckBox1 As System.Windows.Forms.CheckBox
        Private WithEvents CheckBox2 As System.Windows.Forms.CheckBox
        Private WithEvents CheckBox3 As System.Windows.Forms.CheckBox
        Private WithEvents CheckBox4 As System.Windows.Forms.CheckBox
        Private WithEvents _DateDateTimePicker As System.Windows.Forms.DateTimePicker
        Private WithEvents savePictureBox As System.Windows.Forms.PictureBox
        Private WithEvents pasteImageButton As System.Windows.Forms.Button
        Private WithEvents exitButton As System.Windows.Forms.Button
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.freeFormTextBox = New System.Windows.Forms.TextBox
            Me.fontButton = New System.Windows.Forms.Button
            Me.GroupBox1 = New System.Windows.Forms.GroupBox
            Me.CheckBox4 = New System.Windows.Forms.CheckBox
            Me.CheckBox3 = New System.Windows.Forms.CheckBox
            Me.CheckBox2 = New System.Windows.Forms.CheckBox
            Me.CheckBox1 = New System.Windows.Forms.CheckBox
            Me._DateDateTimePicker = New System.Windows.Forms.DateTimePicker
            Me.savePictureBox = New System.Windows.Forms.PictureBox
            Me.pasteImageButton = New System.Windows.Forms.Button
            Me.exitButton = New System.Windows.Forms.Button
            Me.GroupBox1.SuspendLayout()
            Me.SuspendLayout()
            '
            'freeFormTextBox
            '
            Me.freeFormTextBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or
                                                System.Windows.Forms.AnchorStyles.Left) Or
                                            System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.freeFormTextBox.Location = New System.Drawing.Point(8, 8)
            Me.freeFormTextBox.Multiline = True
            Me.freeFormTextBox.Name = "freeFormTextBox"
            Me.freeFormTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.freeFormTextBox.Size = New System.Drawing.Size(468, 173)
            Me.freeFormTextBox.TabIndex = 0
            Me.freeFormTextBox.Text = "Type something here..."
            '
            'fontButton
            '
            Me.fontButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.fontButton.Location = New System.Drawing.Point(8, 189)
            Me.fontButton.Name = "fontButton"
            Me.fontButton.Size = New System.Drawing.Size(56, 24)
            Me.fontButton.TabIndex = 1
            Me.fontButton.Text = "Font..."
            '
            'GroupBox1
            '
            Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
            Me.GroupBox1.Controls.Add(Me.CheckBox4)
            Me.GroupBox1.Controls.Add(Me.CheckBox3)
            Me.GroupBox1.Controls.Add(Me.CheckBox2)
            Me.GroupBox1.Controls.Add(Me.CheckBox1)
            Me.GroupBox1.Location = New System.Drawing.Point(8, 221)
            Me.GroupBox1.Name = "GroupBox1"
            Me.GroupBox1.Size = New System.Drawing.Size(112, 128)
            Me.GroupBox1.TabIndex = 2
            Me.GroupBox1.TabStop = False
            Me.GroupBox1.Text = "Some Options"
            '
            'CheckBox4
            '
            Me.CheckBox4.Location = New System.Drawing.Point(8, 96)
            Me.CheckBox4.Name = "CheckBox4"
            Me.CheckBox4.Size = New System.Drawing.Size(96, 16)
            Me.CheckBox4.TabIndex = 3
            Me.CheckBox4.Text = "CheckBox4"
            Me.CheckBox4.ThreeState = True
            '
            'CheckBox3
            '
            Me.CheckBox3.Location = New System.Drawing.Point(8, 72)
            Me.CheckBox3.Name = "CheckBox3"
            Me.CheckBox3.Size = New System.Drawing.Size(96, 16)
            Me.CheckBox3.TabIndex = 2
            Me.CheckBox3.Text = "CheckBox3"
            Me.CheckBox3.ThreeState = True
            '
            'CheckBox2
            '
            Me.CheckBox2.Location = New System.Drawing.Point(8, 48)
            Me.CheckBox2.Name = "CheckBox2"
            Me.CheckBox2.Size = New System.Drawing.Size(96, 16)
            Me.CheckBox2.TabIndex = 1
            Me.CheckBox2.Text = "CheckBox2"
            Me.CheckBox2.ThreeState = True
            '
            'CheckBox1
            '
            Me.CheckBox1.Location = New System.Drawing.Point(8, 24)
            Me.CheckBox1.Name = "CheckBox1"
            Me.CheckBox1.Size = New System.Drawing.Size(96, 16)
            Me.CheckBox1.TabIndex = 0
            Me.CheckBox1.Text = "CheckBox1"
            Me.CheckBox1.ThreeState = True
            '
            'dateDateTimePicker
            '
            Me._DateDateTimePicker.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me._DateDateTimePicker.Location = New System.Drawing.Point(276, 193)
            Me._DateDateTimePicker.Name = "dateDateTimePicker"
            Me._DateDateTimePicker.TabIndex = 3
            '
            'savePictureBox
            '
            Me.savePictureBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) Or
                                              System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.savePictureBox.Location = New System.Drawing.Point(128, 221)
            Me.savePictureBox.Name = "savePictureBox"
            Me.savePictureBox.Size = New System.Drawing.Size(348, 100)
            Me.savePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
            Me.savePictureBox.TabIndex = 4
            Me.savePictureBox.TabStop = False
            '
            'pasteImageButton
            '
            Me.pasteImageButton.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) Or
                                                System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            Me.pasteImageButton.Location = New System.Drawing.Point(128, 325)
            Me.pasteImageButton.Name = "pasteImageButton"
            Me.pasteImageButton.Size = New System.Drawing.Size(352, 24)
            Me.pasteImageButton.TabIndex = 5
            Me.pasteImageButton.Text = "Paste Image from Clipboard"
            '
            'exitButton
            '
            Me.exitButton.Location = New System.Drawing.Point(168, 184)
            Me.exitButton.Name = "exitButton"
            Me.exitButton.Size = New System.Drawing.Size(88, 32)
            Me.exitButton.TabIndex = 6
            Me.exitButton.Text = "E&xit"
            Me.exitButton.Visible = False
            '
            'TestPanel
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
            Me.ClientSize = New System.Drawing.Size(488, 358)
            Me.Controls.Add(Me.exitButton)
            Me.Controls.Add(Me.pasteImageButton)
            Me.Controls.Add(Me.savePictureBox)
            Me.Controls.Add(Me._DateDateTimePicker)
            Me.Controls.Add(Me.GroupBox1)
            Me.Controls.Add(Me.fontButton)
            Me.Controls.Add(Me.freeFormTextBox)
            Me.Name = "TestPanel"
            Me.Text = "Window and User Settings Demo"
            Me.GroupBox1.ResumeLayout(False)
            Me.ResumeLayout(False)

        End Sub

#End Region

#Region " METHODS "

#End Region

#Region " FORM EVENT HANDLERS "

        ''' <summary>Occurs when the form is loaded.</summary>
        ''' <param name="sender"><see cref="System.Object"/> instance of this 
        '''   <see cref="System.Windows.Forms.Form"/></param>
        ''' <param name="e"><see cref="System.EventArgs"/></param>
        ''' <remarks>Use this method for doing any final initialization right before 
        '''   the form is shown.  This is a good place to change the Visible and
        '''   ShowInTaskbar properties to start the form as hidden.  
        '''   Starting a form as hidden is useful for forms that need to be running but that
        '''   should not show themselves right away, such as forms with a notify icon in the
        '''   task bar.</remarks>
        Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            Try

                ' Turn on the form hourglass cursor
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

                ' set the form caption
                Me.Text = isr.Core.AssemblyInfoExtensions.ExtendedCaption(My.Application.Info) & ": USER PREFERENCES TESTER"

                ' center the form
                Me.CenterToScreen()

            Catch

                ' Use throw without an argument in order to preserve the stack location 
                ' where the exception was initially raised.
                Throw

            Finally

                ' Turn off the form hourglass cursor
                Me.Cursor = System.Windows.Forms.Cursors.Default

            End Try

        End Sub

        Private Sub form_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated
            ' Me.notesMessageList.PushMessage("Activated")
        End Sub

#End Region

#Region " Overridden Methods "

        ''' <summary>Occurs when the form loads.</summary>
        ''' <remarks>Use this method to load settings from the User-Specific settings file
        ''' </remarks>
        ''' <history date="09/09/03" by="David" revision="1.0.1347.x">
        '''  Create
        ''' </history>
        Protected Overrides Sub OnLoadSettings()

            ' Let MyBase do its thing...
            MyBase.OnLoadSettings()

            ' Load my form's data:
            ' TextBox attributes: text (String), font (Object), color (Structure)
            freeFormTextBox.ForeColor = CType(MyBase.Scriber.Fetch("freeFormTextBox.ForeColor", freeFormTextBox.ForeColor), Color)
            freeFormTextBox.Font = CType(MyBase.Scriber.Fetch("freeFormTextBox.Font", freeFormTextBox.Font), Font)
            freeFormTextBox.Text = CType(MyBase.Scriber.Fetch("freeFormTextBox.Text", freeFormTextBox.Text), String)

            ' CheckState (Enum)
            CheckBox1.CheckState = CType(MyBase.Scriber.Fetch("CheckBox1.CheckState", CheckBox1.CheckState), CheckState)
            CheckBox2.CheckState = CType(MyBase.Scriber.Fetch("CheckBox2.CheckState", CheckBox2.CheckState), CheckState)
            CheckBox3.CheckState = CType(MyBase.Scriber.Fetch("CheckBox3.CheckState", CheckBox3.CheckState), CheckState)
            CheckBox4.CheckState = CType(MyBase.Scriber.Fetch("CheckBox4.CheckState", CheckBox4.CheckState), CheckState)

            ' Date time
            Me._DateDateTimePicker.Value = CType(MyBase.Scriber.Fetch("_DateDateTimePicker.Value", Date.Now), DateTime)

            ' Image (Bitmap)
            savePictureBox.Image = CType(MyBase.Scriber.Fetch("savePictureBox.Image", Nothing), Image)

        End Sub

        ''' <summary>Occurs when the form unloads.</summary>
        ''' <remarks>Use this method to save settings to the User-Specific settings file
        ''' </remarks>
        ''' <history date="09/09/03" by="David" revision="1.0.1347.x">
        '''  Create
        ''' </history>
        Protected Overrides Sub OnSaveSettings()

            ' Let my base do its thing...
            MyBase.OnSaveSettings()

            ' Save my form's data:
            MyBase.Scriber.Update("freeFormTextBox.ForeColor", freeFormTextBox.ForeColor, True)
            MyBase.Scriber.Update("freeFormTextBox.Font", freeFormTextBox.Font, True)
            MyBase.Scriber.Update("freeFormTextBox.Text", freeFormTextBox.Text, True)

            MyBase.Scriber.Update("CheckBox1.CheckState", CheckBox1.CheckState, True)
            MyBase.Scriber.Update("CheckBox2.CheckState", CheckBox2.CheckState, True)
            MyBase.Scriber.Update("CheckBox3.CheckState", CheckBox3.CheckState, True)
            MyBase.Scriber.Update("CheckBox4.CheckState", CheckBox4.CheckState, True)

            MyBase.Scriber.Update("_DateDateTimePicker.Value", Me._DateDateTimePicker.Value, True)

            MyBase.Scriber.Update("savePictureBox.Image", savePictureBox.Image, True)

            ' Since this is the "main" window, save
            MyBase.Scriber.WriteAll()

        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

        Private Sub fontButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fontButton.Click
            ' open font dialog:
            Using dlg As New FontDialog
                dlg.Color = freeFormTextBox.ForeColor
                dlg.Font = freeFormTextBox.Font
                dlg.ShowColor = True
                dlg.ShowEffects = True
                dlg.AllowSimulations = True
                dlg.ShowApply = False
                If dlg.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
                    freeFormTextBox.ForeColor = dlg.Color
                    freeFormTextBox.Font = dlg.Font
                End If
            End Using
        End Sub

        Private Sub pasteImageButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pasteImageButton.Click
            If Clipboard.GetDataObject.GetDataPresent(GetType(Bitmap)) Then
                Dim bmp As Bitmap = CType(Clipboard.GetDataObject.GetData(GetType(Bitmap)), Bitmap)
                savePictureBox.Image = bmp
            Else
                MessageBox.Show("No Bitmap data present in clipboard.", "Clipboard", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End Sub

        ''' <summary>Closes the form and exits the application.</summary>
        Private Sub exitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitButton.Click

            Try

                'Me.statusLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "{0} {1}", Date.Now.Ticks, Gopher.UsingDevices.ToString)
                Me.Close()

            Catch ex As isr.Configuration.BaseException

                If My.MyApplication.ProcessException(ex, "Exception occurred Closing", isr.WindowsForms.ExceptionDisplayButtons.Continue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            End Try

            Exit Sub

        End Sub

#End Region

    End Class
End Namespace
