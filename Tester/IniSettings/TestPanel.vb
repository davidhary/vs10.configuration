Namespace IniSettings
    ''' <summary>Includes code for form TestPanel.</summary>
    ''' <license>
    ''' (c) 2003 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="09/02/03" by="David" revision="2.0.1340.x">
    '''  Created
    ''' </history>
    Friend Class TestPanel
        Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "
        Public Sub New()
            MyBase.New()

            'This call is required by the Windows Form Designer.
            InitializeComponent()
        End Sub
        ' Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

            Try

                If disposing Then

                    If components IsNot Nothing Then
                        components.Dispose()
                    End If


                End If

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try

        End Sub
        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer
        Private WithEvents exitButton As System.Windows.Forms.Button
        Private WithEvents settingTextBox As System.Windows.Forms.TextBox
        Private WithEvents sectionTextBox As System.Windows.Forms.TextBox
        Private WithEvents _FilePathTextBox As System.Windows.Forms.TextBox
        Private WithEvents writeSectionButton As System.Windows.Forms.Button
        Private WithEvents readSectionButton As System.Windows.Forms.Button
        Private WithEvents writeSettingsButton As System.Windows.Forms.Button
        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.
        'Do not modify it using the code editor.
        Private WithEvents readSettingsButton As System.Windows.Forms.Button
        Private WithEvents settingNameComboBox As System.Windows.Forms.ComboBox
        Private WithEvents defaultValueTextBox As System.Windows.Forms.TextBox
        Private WithEvents dataTypeComboBox As System.Windows.Forms.ComboBox
        Private WithEvents formatTextBox As System.Windows.Forms.TextBox
        Private WithEvents sectionNameTextBox As System.Windows.Forms.TextBox
        Private WithEvents sectionLabel As System.Windows.Forms.Label
        Private WithEvents valueLabel As System.Windows.Forms.Label
        Private WithEvents _FilePathTextBoxLbel As System.Windows.Forms.Label
        Private WithEvents defaultValueLabel As System.Windows.Forms.Label
        Private WithEvents dataTypeLabel As System.Windows.Forms.Label
        Private WithEvents formatLabel As System.Windows.Forms.Label
        Private WithEvents settingNameLabel As System.Windows.Forms.Label
        Private WithEvents sectionNameLabel As System.Windows.Forms.Label
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container
            Me.exitButton = New System.Windows.Forms.Button
            Me.settingTextBox = New System.Windows.Forms.TextBox
            Me.sectionTextBox = New System.Windows.Forms.TextBox
            Me.sectionLabel = New System.Windows.Forms.Label
            Me.valueLabel = New System.Windows.Forms.Label
            Me._FilePathTextBox = New System.Windows.Forms.TextBox
            Me._FilePathTextBoxLbel = New System.Windows.Forms.Label
            Me.writeSectionButton = New System.Windows.Forms.Button
            Me.readSectionButton = New System.Windows.Forms.Button
            Me.writeSettingsButton = New System.Windows.Forms.Button
            Me.readSettingsButton = New System.Windows.Forms.Button
            Me.settingNameComboBox = New System.Windows.Forms.ComboBox
            Me.defaultValueTextBox = New System.Windows.Forms.TextBox
            Me.dataTypeComboBox = New System.Windows.Forms.ComboBox
            Me.formatTextBox = New System.Windows.Forms.TextBox
            Me.sectionNameTextBox = New System.Windows.Forms.TextBox
            Me.defaultValueLabel = New System.Windows.Forms.Label
            Me.dataTypeLabel = New System.Windows.Forms.Label
            Me.formatLabel = New System.Windows.Forms.Label
            Me.settingNameLabel = New System.Windows.Forms.Label
            Me.sectionNameLabel = New System.Windows.Forms.Label
            Me.SuspendLayout()
            '
            'exitButton
            '
            Me.exitButton.BackColor = System.Drawing.SystemColors.Control
            Me.exitButton.Cursor = System.Windows.Forms.Cursors.Default
            Me.exitButton.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.exitButton.ForeColor = System.Drawing.SystemColors.ControlText
            Me.exitButton.Location = New System.Drawing.Point(496, 304)
            Me.exitButton.Name = "exitButton"
            Me.exitButton.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.exitButton.Size = New System.Drawing.Size(97, 29)
            Me.exitButton.TabIndex = 21
            Me.exitButton.Text = "E&xit"
            '
            'settingTextBox
            '
            Me.settingTextBox.AcceptsReturn = True
            Me.settingTextBox.AutoSize = False
            Me.settingTextBox.BackColor = System.Drawing.SystemColors.Window
            Me.settingTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
            Me.settingTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.settingTextBox.ForeColor = System.Drawing.SystemColors.WindowText
            Me.settingTextBox.Location = New System.Drawing.Point(351, 120)
            Me.settingTextBox.MaxLength = 0
            Me.settingTextBox.Name = "settingTextBox"
            Me.settingTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.settingTextBox.Size = New System.Drawing.Size(156, 21)
            Me.settingTextBox.TabIndex = 13
            Me.settingTextBox.Text = ""
            '
            'sectionTextBox
            '
            Me.sectionTextBox.AcceptsReturn = True
            Me.sectionTextBox.AutoSize = False
            Me.sectionTextBox.BackColor = System.Drawing.SystemColors.Window
            Me.sectionTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
            Me.sectionTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.sectionTextBox.ForeColor = System.Drawing.SystemColors.WindowText
            Me.sectionTextBox.Location = New System.Drawing.Point(16, 170)
            Me.sectionTextBox.MaxLength = 32000
            Me.sectionTextBox.Multiline = True
            Me.sectionTextBox.Name = "sectionTextBox"
            Me.sectionTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.sectionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.sectionTextBox.Size = New System.Drawing.Size(576, 120)
            Me.sectionTextBox.TabIndex = 17
            Me.sectionTextBox.Text = ""
            '
            'sectionLabel
            '
            Me.sectionLabel.BackColor = System.Drawing.SystemColors.Control
            Me.sectionLabel.Cursor = System.Windows.Forms.Cursors.Default
            Me.sectionLabel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.sectionLabel.ForeColor = System.Drawing.SystemColors.ControlText
            Me.sectionLabel.Location = New System.Drawing.Point(16, 152)
            Me.sectionLabel.Name = "sectionLabel"
            Me.sectionLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.sectionLabel.Size = New System.Drawing.Size(60, 16)
            Me.sectionLabel.TabIndex = 16
            Me.sectionLabel.Text = "Section:"
            '
            'valueLabel
            '
            Me.valueLabel.BackColor = System.Drawing.SystemColors.Control
            Me.valueLabel.Cursor = System.Windows.Forms.Cursors.Default
            Me.valueLabel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.valueLabel.ForeColor = System.Drawing.SystemColors.ControlText
            Me.valueLabel.Location = New System.Drawing.Point(351, 104)
            Me.valueLabel.Name = "valueLabel"
            Me.valueLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.valueLabel.Size = New System.Drawing.Size(56, 16)
            Me.valueLabel.TabIndex = 12
            Me.valueLabel.Text = "Value: "
            Me.valueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            '_FilePathTextBox
            '
            Me._FilePathTextBox.AcceptsReturn = True
            Me._FilePathTextBox.AutoSize = False
            Me._FilePathTextBox.BackColor = System.Drawing.SystemColors.Window
            Me._FilePathTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
            Me._FilePathTextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._FilePathTextBox.ForeColor = System.Drawing.SystemColors.WindowText
            Me._FilePathTextBox.Location = New System.Drawing.Point(16, 24)
            Me._FilePathTextBox.MaxLength = 0
            Me._FilePathTextBox.Name = "_FilePathTextBox"
            Me._FilePathTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me._FilePathTextBox.Size = New System.Drawing.Size(576, 20)
            Me._FilePathTextBox.TabIndex = 1
            Me._FilePathTextBox.Text = ""
            '
            '_FilePathTextBoxLbel
            '
            Me._FilePathTextBoxLbel.BackColor = System.Drawing.SystemColors.Control
            Me._FilePathTextBoxLbel.Cursor = System.Windows.Forms.Cursors.Default
            Me._FilePathTextBoxLbel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._FilePathTextBoxLbel.ForeColor = System.Drawing.SystemColors.ControlText
            Me._FilePathTextBoxLbel.Location = New System.Drawing.Point(16, 4)
            Me._FilePathTextBoxLbel.Name = "_FilePathTextBoxLbel"
            Me._FilePathTextBoxLbel.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me._FilePathTextBoxLbel.Size = New System.Drawing.Size(90, 16)
            Me._FilePathTextBoxLbel.TabIndex = 0
            Me._FilePathTextBoxLbel.Text = "&File Name: "
            Me._FilePathTextBoxLbel.TextAlign = System.Drawing.ContentAlignment.BottomLeft
            '
            'writeSectionButton
            '
            Me.writeSectionButton.BackColor = System.Drawing.SystemColors.Control
            Me.writeSectionButton.Cursor = System.Windows.Forms.Cursors.Default
            Me.writeSectionButton.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.writeSectionButton.ForeColor = System.Drawing.SystemColors.ControlText
            Me.writeSectionButton.Location = New System.Drawing.Point(136, 304)
            Me.writeSectionButton.Name = "writeSectionButton"
            Me.writeSectionButton.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.writeSectionButton.Size = New System.Drawing.Size(112, 29)
            Me.writeSectionButton.TabIndex = 19
            Me.writeSectionButton.Text = "Wri&te Section"
            '
            'readSectionButton
            '
            Me.readSectionButton.BackColor = System.Drawing.SystemColors.Control
            Me.readSectionButton.Cursor = System.Windows.Forms.Cursors.Default
            Me.readSectionButton.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.readSectionButton.ForeColor = System.Drawing.SystemColors.ControlText
            Me.readSectionButton.Location = New System.Drawing.Point(16, 304)
            Me.readSectionButton.Name = "readSectionButton"
            Me.readSectionButton.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.readSectionButton.Size = New System.Drawing.Size(112, 29)
            Me.readSectionButton.TabIndex = 18
            Me.readSectionButton.Text = "R&ead Section"
            '
            'writeSettingsButton
            '
            Me.writeSettingsButton.BackColor = System.Drawing.SystemColors.Control
            Me.writeSettingsButton.Cursor = System.Windows.Forms.Cursors.Default
            Me.writeSettingsButton.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.writeSettingsButton.ForeColor = System.Drawing.SystemColors.ControlText
            Me.writeSettingsButton.Location = New System.Drawing.Point(520, 114)
            Me.writeSettingsButton.Name = "writeSettingsButton"
            Me.writeSettingsButton.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.writeSettingsButton.Size = New System.Drawing.Size(72, 32)
            Me.writeSettingsButton.TabIndex = 15
            Me.writeSettingsButton.Text = "&Write"
            '
            'readSettingsButton
            '
            Me.readSettingsButton.BackColor = System.Drawing.SystemColors.Control
            Me.readSettingsButton.Cursor = System.Windows.Forms.Cursors.Default
            Me.readSettingsButton.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.readSettingsButton.ForeColor = System.Drawing.SystemColors.ControlText
            Me.readSettingsButton.Location = New System.Drawing.Point(520, 67)
            Me.readSettingsButton.Name = "readSettingsButton"
            Me.readSettingsButton.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.readSettingsButton.Size = New System.Drawing.Size(72, 32)
            Me.readSettingsButton.TabIndex = 14
            Me.readSettingsButton.Text = "&Read"
            '
            'settingNameComboBox
            '
            Me.settingNameComboBox.Items.AddRange(New Object() {"Boolean", "Byte", "Date", "Decimal", "Double", "Int32", "Int64", "Int16", "Single", "String"})
            Me.settingNameComboBox.Location = New System.Drawing.Point(16, 120)
            Me.settingNameComboBox.Name = "settingNameComboBox"
            Me.settingNameComboBox.Size = New System.Drawing.Size(156, 22)
            Me.settingNameComboBox.TabIndex = 9
            Me.settingNameComboBox.Text = "ComboBox1"
            '
            'defaultValueTextBox
            '
            Me.defaultValueTextBox.AcceptsReturn = True
            Me.defaultValueTextBox.AutoSize = False
            Me.defaultValueTextBox.BackColor = System.Drawing.SystemColors.Window
            Me.defaultValueTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
            Me.defaultValueTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.defaultValueTextBox.ForeColor = System.Drawing.SystemColors.WindowText
            Me.defaultValueTextBox.Location = New System.Drawing.Point(184, 120)
            Me.defaultValueTextBox.MaxLength = 0
            Me.defaultValueTextBox.Name = "defaultValueTextBox"
            Me.defaultValueTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.defaultValueTextBox.Size = New System.Drawing.Size(156, 21)
            Me.defaultValueTextBox.TabIndex = 11
            Me.defaultValueTextBox.Text = ""
            '
            'dataTypeComboBox
            '
            Me.dataTypeComboBox.BackColor = System.Drawing.SystemColors.Window
            Me.dataTypeComboBox.Cursor = System.Windows.Forms.Cursors.Default
            Me.dataTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.dataTypeComboBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.dataTypeComboBox.ForeColor = System.Drawing.SystemColors.WindowText
            Me.dataTypeComboBox.Location = New System.Drawing.Point(351, 72)
            Me.dataTypeComboBox.Name = "dataTypeComboBox"
            Me.dataTypeComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.dataTypeComboBox.Size = New System.Drawing.Size(156, 22)
            Me.dataTypeComboBox.TabIndex = 7
            '
            'formatTextBox
            '
            Me.formatTextBox.AcceptsReturn = True
            Me.formatTextBox.AutoSize = False
            Me.formatTextBox.BackColor = System.Drawing.SystemColors.Window
            Me.formatTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
            Me.formatTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.formatTextBox.ForeColor = System.Drawing.SystemColors.WindowText
            Me.formatTextBox.Location = New System.Drawing.Point(184, 72)
            Me.formatTextBox.MaxLength = 0
            Me.formatTextBox.Name = "formatTextBox"
            Me.formatTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.formatTextBox.Size = New System.Drawing.Size(156, 21)
            Me.formatTextBox.TabIndex = 5
            Me.formatTextBox.Text = ""
            '
            'sectionNameTextBox
            '
            Me.sectionNameTextBox.AcceptsReturn = True
            Me.sectionNameTextBox.AutoSize = False
            Me.sectionNameTextBox.BackColor = System.Drawing.SystemColors.Window
            Me.sectionNameTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
            Me.sectionNameTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.sectionNameTextBox.ForeColor = System.Drawing.SystemColors.WindowText
            Me.sectionNameTextBox.Location = New System.Drawing.Point(16, 72)
            Me.sectionNameTextBox.MaxLength = 0
            Me.sectionNameTextBox.Name = "sectionNameTextBox"
            Me.sectionNameTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.sectionNameTextBox.Size = New System.Drawing.Size(156, 21)
            Me.sectionNameTextBox.TabIndex = 3
            Me.sectionNameTextBox.Text = ""
            '
            'defaultValueLabel
            '
            Me.defaultValueLabel.BackColor = System.Drawing.SystemColors.Control
            Me.defaultValueLabel.Cursor = System.Windows.Forms.Cursors.Default
            Me.defaultValueLabel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.defaultValueLabel.ForeColor = System.Drawing.SystemColors.ControlText
            Me.defaultValueLabel.Location = New System.Drawing.Point(184, 104)
            Me.defaultValueLabel.Name = "defaultValueLabel"
            Me.defaultValueLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.defaultValueLabel.Size = New System.Drawing.Size(90, 16)
            Me.defaultValueLabel.TabIndex = 10
            Me.defaultValueLabel.Text = "&Default Value: "
            Me.defaultValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'dataTypeLabel
            '
            Me.dataTypeLabel.BackColor = System.Drawing.SystemColors.Control
            Me.dataTypeLabel.Cursor = System.Windows.Forms.Cursors.Default
            Me.dataTypeLabel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.dataTypeLabel.ForeColor = System.Drawing.SystemColors.ControlText
            Me.dataTypeLabel.Location = New System.Drawing.Point(351, 56)
            Me.dataTypeLabel.Name = "dataTypeLabel"
            Me.dataTypeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.dataTypeLabel.Size = New System.Drawing.Size(90, 16)
            Me.dataTypeLabel.TabIndex = 6
            Me.dataTypeLabel.Text = "Data T&ype: "
            Me.dataTypeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'formatLabel
            '
            Me.formatLabel.BackColor = System.Drawing.SystemColors.Control
            Me.formatLabel.Cursor = System.Windows.Forms.Cursors.Default
            Me.formatLabel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.formatLabel.ForeColor = System.Drawing.SystemColors.ControlText
            Me.formatLabel.Location = New System.Drawing.Point(184, 56)
            Me.formatLabel.Name = "formatLabel"
            Me.formatLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.formatLabel.Size = New System.Drawing.Size(90, 16)
            Me.formatLabel.TabIndex = 4
            Me.formatLabel.Text = "&Format: "
            Me.formatLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'settingNameLabel
            '
            Me.settingNameLabel.BackColor = System.Drawing.SystemColors.Control
            Me.settingNameLabel.Cursor = System.Windows.Forms.Cursors.Default
            Me.settingNameLabel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.settingNameLabel.ForeColor = System.Drawing.SystemColors.ControlText
            Me.settingNameLabel.Location = New System.Drawing.Point(16, 104)
            Me.settingNameLabel.Name = "settingNameLabel"
            Me.settingNameLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.settingNameLabel.Size = New System.Drawing.Size(90, 16)
            Me.settingNameLabel.TabIndex = 8
            Me.settingNameLabel.Text = "Sett&ing Name: "
            Me.settingNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'sectionNameLabel
            '
            Me.sectionNameLabel.BackColor = System.Drawing.SystemColors.Control
            Me.sectionNameLabel.Cursor = System.Windows.Forms.Cursors.Default
            Me.sectionNameLabel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.sectionNameLabel.ForeColor = System.Drawing.SystemColors.ControlText
            Me.sectionNameLabel.Location = New System.Drawing.Point(16, 56)
            Me.sectionNameLabel.Name = "sectionNameLabel"
            Me.sectionNameLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.sectionNameLabel.Size = New System.Drawing.Size(90, 16)
            Me.sectionNameLabel.TabIndex = 2
            Me.sectionNameLabel.Text = "&Section Name: "
            Me.sectionNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'TestPanel
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
            Me.ClientSize = New System.Drawing.Size(604, 342)
            Me.Controls.Add(Me.settingNameComboBox)
            Me.Controls.Add(Me.defaultValueTextBox)
            Me.Controls.Add(Me.formatTextBox)
            Me.Controls.Add(Me.sectionNameTextBox)
            Me.Controls.Add(Me._FilePathTextBox)
            Me.Controls.Add(Me.settingTextBox)
            Me.Controls.Add(Me.sectionTextBox)
            Me.Controls.Add(Me.dataTypeComboBox)
            Me.Controls.Add(Me.defaultValueLabel)
            Me.Controls.Add(Me.dataTypeLabel)
            Me.Controls.Add(Me.formatLabel)
            Me.Controls.Add(Me.settingNameLabel)
            Me.Controls.Add(Me.sectionNameLabel)
            Me.Controls.Add(Me.exitButton)
            Me.Controls.Add(Me.writeSectionButton)
            Me.Controls.Add(Me.readSectionButton)
            Me.Controls.Add(Me.writeSettingsButton)
            Me.Controls.Add(Me.readSettingsButton)
            Me.Controls.Add(Me._FilePathTextBoxLbel)
            Me.Controls.Add(Me.valueLabel)
            Me.Controls.Add(Me.sectionLabel)
            Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.KeyPreview = True
            Me.Location = New System.Drawing.Point(236, 160)
            Me.MaximizeBox = False
            Me.Name = "TestPanel"
            Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
            Me.ResumeLayout(False)

        End Sub

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        ''' <summary>Initializes the class objects.</summary>
        ''' <exception cref="ApplicationException" guarantee="strong">
        '''   failed instantiating objects.</exception>
        ''' <remarks>Called from the form load method to instantiate 
        '''   module-level objects.</remarks>
        Private Sub instantiateObjects()

            ' Set the combo box data.
            dataTypeComboBox.Items.Clear()

            dataTypeComboBox.Items.Add(GetType(System.Boolean))
            dataTypeComboBox.Items.Add(GetType(System.Byte))
            dataTypeComboBox.Items.Add(GetType(System.DateTime))
            dataTypeComboBox.Items.Add(GetType(System.Decimal))
            dataTypeComboBox.Items.Add(GetType(System.Double))
            dataTypeComboBox.Items.Add(GetType(System.Int16))
            dataTypeComboBox.Items.Add(GetType(System.Int32))
            dataTypeComboBox.Items.Add(GetType(System.Int64))
            dataTypeComboBox.Items.Add(GetType(System.Single))
            dataTypeComboBox.Items.Add(GetType(System.String))
            dataTypeComboBox.SelectedIndex = 0

            ' set the default INI Settings (INI) file name
            _FilePathTextBox.Text = isr.Configuration.ProfileScriber.DefaultFilePath()

            ' Set the default section
            sectionNameTextBox.Text = "Section"

            ' Set the default setting name
            settingNameComboBox.Text = "Boolean"

            ' Set the default value
            defaultValueTextBox.Text = "0"

            ' Set the default format
            formatTextBox.Text = "0"

        End Sub

#End Region

#Region " METHODS "

        ''' <summary>Reads a setting from the INI Settings file.</summary>
        ''' <remarks>Use this method to learn how to read a setting
        '''   from the INI Settings file based on the selection on the form.
        '''   Note the difference between this method and normal operation that
        '''   will not call on using the .ConvertToType method as the type is 
        '''   not picked up from a combo box but is already known to the programmer.
        ''' </remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _read()

            Try

                ' Declare the isrSettings object
                Using ini As isr.Configuration.PrivateProfileScriber = New isr.Configuration.PrivateProfileScriber()

                    ' Set the file name.  If the text data are
                    ' empty, WIN.INI is used by default
                    ini.FilePath = _FilePathTextBox.Text

                    ' set the section name
                    ini.SectionName = sectionNameTextBox.Text

                    ' Set the setting name
                    ini.SettingName = settingNameComboBox.Text

                    ' get the data type
                    ini.SettingType = CType(Me.dataTypeComboBox.SelectedItem, Type)

                    ' set the default value to the correct type.
                    Try
                        ini.DefaultValue = isr.Configuration.Conversions.Deserialize(defaultValueTextBox.Text, ini.SettingType)
                    Catch ex As FormatException
                        If My.MyApplication.ProcessException(ex, "Default string cannot be converted to the selected format", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                            Me.Close()
                        End If
                    End Try

                    ' Read the setting and place in results box
                    settingTextBox.Text = ini.SettingValue.ToString

                End Using

            Catch ex As isr.Configuration.BaseException

                ' Display the error message
                If My.MyApplication.ProcessException(ex, "Exception occurred reading setting", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            Catch ex As System.InvalidCastException

                ' Display the error message
                If My.MyApplication.ProcessException(ex, "Exception occurred reading setting", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            Catch ex As Exception

                If My.MyApplication.ProcessException(ex, "Unhandled exception", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            End Try

        End Sub

        ''' <summary>Reads a section from the INI Settings file.</summary>
        ''' <remarks>Use this method to learn how to read a section
        '''   from the INI Settings file based. Replaces zero characters in the
        '''   section string with zeroStandInChar values before the section string 
        '''   is displayed in the text box.
        ''' </remarks>
        Private Sub readSection()

            ' Declare the isrSettings object
            Dim ini As isr.Configuration.PrivateProfileScriber

            ' Create an instance of the isrSettings class
            ini = New isr.Configuration.PrivateProfileScriber()

            Try

                ' set the file name.  If the text data are empty, the WIN.INI file is used
                ini.FilePath = _FilePathTextBox.Text

                ' set the section name
                ini.SectionName = sectionNameTextBox.Text

                ' declare a string collection for the section data
                Dim sectionData As System.Collections.Specialized.StringCollection = ini.SectionCollection
                Dim sectionItem As String
                Dim sectionText As New System.Text.StringBuilder
                For Each sectionItem In sectionData
                    sectionText.Append(sectionItem)
                    sectionText.Append(Environment.NewLine)
                Next
                Me.sectionTextBox.Text = sectionText.ToString

            Catch ex As isr.Configuration.BaseException

                ' Display the error message
                If My.MyApplication.ProcessException(ex, "Exception occurred reading section", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            Finally

                ' remove the instance of the INI Settings Scriber Class
                ini.Dispose()
                ini = Nothing

            End Try

        End Sub

        ''' <summary>Writes a setting to the INI Settings file.</summary>
        ''' <remarks>Use this method to learn how to write a setting
        '''   to the INI Settings file based on the selection on the form.
        ''' </remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub writeSetting()

            ' Declare the isrSettings object
            Dim ini As isr.Configuration.PrivateProfileScriber

            ' Create an instance of the isrSettings class
            ini = New isr.Configuration.PrivateProfileScriber()

            Try

                ' set the file name.  If the text data are empty, the WIN.INI file is used
                ini.FilePath = _FilePathTextBox.Text

                ' set the section name
                ini.SectionName = sectionNameTextBox.Text

                ' set the setting name
                ini.SettingName = settingNameComboBox.Text

                ' get the data type
                ini.SettingType = CType(Me.dataTypeComboBox.SelectedItem, Type)

                ' save the value
                ini.SettingValue = isr.Configuration.Conversions.Deserialize(settingTextBox.Text, CType(Me.dataTypeComboBox.SelectedItem, Type))

            Catch ex As System.InvalidCastException

                ' Display the error message
                If My.MyApplication.ProcessException(ex, "Exception occurred writing setting", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            Catch ex As isr.Configuration.BaseException

                ' Display the error message
                If My.MyApplication.ProcessException(ex, "Exception occurred writing setting", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            Catch ex As Exception

                If My.MyApplication.ProcessException(ex, "Unhandled exception", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            Finally

                ' remove the instance of the INI Settings Scriber Class
                ini.Dispose()
                ini = Nothing

            End Try

        End Sub

        ''' <summary>Writes a section to the INI Settings file.</summary>
        ''' <remarks>Use this method to learn how to write a section
        '''   to the INI Settings file based. This method inserts zero characters in the
        '''   section string in place of zeroStandInChar values before the section string 
        '''   is stored.
        ''' </remarks>
        Private Sub writeSection()

            ' Declare the isrSettings object
            Dim ini As isr.Configuration.PrivateProfileScriber

            ' Create an instance of the isrSettings class
            ini = New isr.Configuration.PrivateProfileScriber()

            Try

                ' exit if WINI.INI is default (i.e., no file is given).
                If _FilePathTextBox.Text.Trim.Length = 0 Then

                    ' You can remove this precaution at you
                    ' own risk!
                    MessageBox.Show("This routine will not replace a section in " & "the WIN.INI file")
                    Exit Sub

                End If

                ' set the file name.  If the text data are empty, the WIN.INI file is used
                ini.FilePath = _FilePathTextBox.Text

                ' set the section name
                ini.SectionName = sectionNameTextBox.Text & "1"

                ' Convert all alternate characters back to zero
                ' and write the section to file
                Dim sectionCollection As System.Collections.Specialized.StringCollection = New System.Collections.Specialized.StringCollection
                Dim sectionItem As String
                For Each sectionItem In sectionTextBox.Text.Split(Environment.NewLine.ToCharArray()(0))
                    sectionCollection.Add(sectionItem.TrimStart(Environment.NewLine.ToCharArray))
                Next
                ini.WriteSection(sectionCollection)

            Catch ex As isr.Configuration.BaseException

                ' Display the error message
                If My.MyApplication.ProcessException(ex, "Exception occurred writing section", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            Finally

                ' remove the instance of the INI Settings Scriber Class
                ini.Dispose()
                ini = Nothing

            End Try

        End Sub

#End Region

#Region " Events "

        ''' <summary>Closes the form and exits the application.</summary>
        ''' <remarks>
        '''     Use this method to close the form and exit this application.
        ''' </remarks>
        ''' <history date="09/02/03" by="David" revision="2.0.1340.x">
        '''  Create
        ''' </history>
        Private Sub exitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitButton.Click
            Me.Close()
            Exit Sub

        End Sub

        Private Sub readSettingsButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readSettingsButton.Click
            Me._read()
        End Sub

        Private Sub readSectionButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readSectionButton.Click
            readSection()
        End Sub

        Private Sub writeSectionButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles writeSectionButton.Click
            writeSection()
        End Sub

        Private Sub writeSettingsButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles writeSettingsButton.Click
            writeSetting()
        End Sub

        ''' <summary>Occurs when the form is loaded.</summary>
        ''' <param name="sender"><see cref="System.Object"/> instance of this 
        '''   <see cref="System.Windows.Forms.Form"/></param>
        ''' <param name="e"><see cref="System.EventArgs"/></param>
        ''' <remarks>Use this method for doing any final initialization right before 
        '''   the form is shown.  This is a good place to change the Visible and
        '''   ShowInTaskbar properties to start the form as hidden.  
        '''   Starting a form as hidden is useful for forms that need to be running but that
        '''   should not show themselves right away, such as forms with a notify icon in the
        '''   task bar.</remarks>
        Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            Try

                ' Turn on the form hourglass cursor
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

                ' instantiate form objects
                Me.instantiateObjects()

                ' set the form caption
                Me.Text = isr.Core.AssemblyInfoExtensions.ExtendedCaption(My.Application.Info) & ": INI TESTER"

                ' center the form
                Me.CenterToScreen()

            Catch

                ' Use throw without an argument in order to preserve the stack location 
                ' where the exception was initially raised.
                Throw

            Finally

                ' Turn off the form hourglass cursor
                Me.Cursor = System.Windows.Forms.Cursors.Default

            End Try

        End Sub

#End Region

    End Class
End Namespace
