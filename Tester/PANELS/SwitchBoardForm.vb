Imports isr.Core
''' <summary>Includes code for form SwitchboardForm, which serves as a switchboard for this program.</summary>
''' <license>
''' (c) 2003 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="07/29/03" by="David" revision="1.0.1305.x">
''' Created
''' </history>
Friend Class SwitchboardForm
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

    End Sub

    ' Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try

            If disposing Then

                If components IsNot Nothing Then
                    components.Dispose()
                End If

            End If
            ' Free shared unmanaged resources

        Finally

            ' Invoke the base class dispose method
            MyBase.Dispose(disposing)

        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Private WithEvents actionsComboBox As System.Windows.Forms.ComboBox
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    Private WithEvents exitButton As System.Windows.Forms.Button
    Private WithEvents _UserPictureBox As System.Windows.Forms.PictureBox
    Private WithEvents openButton As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(SwitchboardForm))
        Me.actionsComboBox = New System.Windows.Forms.ComboBox
        Me._UserPictureBox = New System.Windows.Forms.PictureBox
        Me.exitButton = New System.Windows.Forms.Button
        Me.openButton = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'actionsComboBox
        '
        Me.actionsComboBox.BackColor = System.Drawing.SystemColors.Window
        Me.actionsComboBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.actionsComboBox.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.actionsComboBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.actionsComboBox.Location = New System.Drawing.Point(8, 16)
        Me.actionsComboBox.Name = "actionsComboBox"
        Me.actionsComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.actionsComboBox.Size = New System.Drawing.Size(361, 27)
        Me.actionsComboBox.TabIndex = 2
        Me.actionsComboBox.Text = "Select option from the list"
        '
        '_UserPictureBox
        '
        Me._UserPictureBox.Cursor = System.Windows.Forms.Cursors.Hand
        Me._UserPictureBox.Location = New System.Drawing.Point(11, 149)
        Me._UserPictureBox.Name = "_UserPictureBox"
        Me._UserPictureBox.Size = New System.Drawing.Size(62, 100)
        Me._UserPictureBox.TabIndex = 6
        Me._UserPictureBox.TabStop = False
        '
        'exitButton
        '
        Me.exitButton.Location = New System.Drawing.Point(360, 56)
        Me.exitButton.Name = "exitButton"
        Me.exitButton.TabIndex = 7
        Me.exitButton.Text = "E&xit"
        '
        'openButton
        '
        Me.openButton.Location = New System.Drawing.Point(376, 16)
        Me.openButton.Name = "openButton"
        Me.openButton.Size = New System.Drawing.Size(58, 24)
        Me.openButton.TabIndex = 8
        Me.openButton.Text = "&Open..."
        '
        'SwitchboardForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(448, 94)
        Me.Controls.Add(Me.openButton)
        Me.Controls.Add(Me.exitButton)
        Me.Controls.Add(Me.actionsComboBox)
        Me.Controls.Add(Me._UserPictureBox)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(219, 182)
        Me.MaximizeBox = False
        Me.Name = "SwitchboardForm"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

#End Region

#Region " METHODS  AND  PROPERTIES "

    ''' <summary>Returns true if an instance of the class was created and not disposed.</summary>
    Friend Shared ReadOnly Property Instantiated() As Boolean
        ' Returns true if an instance of the class was created and not disposed
        Get
            Return My.Application.OpenForms.Count > 0 AndAlso 
                My.Application.OpenForms.Item(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name) IsNot Nothing
        End Get
    End Property

        ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A <see cref="System.String">String</see>.</value>
    ''' <remarks>Use this property to get the status message generated by the object.</remarks>
    Private Property statusMessage() As String

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Occurs when the form is loaded.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' instantiate form objects
            Me.instantiateObjects()

            ' set the form caption
            Me.Text = isr.Core.AssemblyInfoExtensions.ExtendedCaption(My.Application.Info) & ": SWITCH BOARD"

            ' center the form
            Me.CenterToScreen()

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary>Enumerates the action options.</summary>
    Private Enum ActionOption
        Application_Configuration
        INI_Settings
        User_Preferences
        Profile
    End Enum

    ''' <summary>Initializes the class objects.</summary>
    ''' <remarks>Called from the form load method to instantiate 
    '''   module-level objects.</remarks>
    Private Sub instantiateObjects()

        ' populate the action list
        Me.populateActiveList()

        ' set registration
        checkPermit()

    End Sub

    ''' <summary>Populates the list of options in the action combo box.</summary>
    ''' <remarks>It seems that out enumerated list does not work very well with
    '''   this list.</remarks>
    ''' <history>
    ''' 	[David] 	11/8/2004	Created
    ''' </history>
    Private Sub populateActiveList()

        ' set the action list
        Dim itemCaption As String
        actionsComboBox.Items.Clear()
        itemCaption = ActionOption.Application_Configuration.ToString
        actionsComboBox.Items.Add(itemCaption.Replace("_", " "))
        itemCaption = ActionOption.INI_Settings.ToString
        actionsComboBox.Items.Add(itemCaption.Replace("_", " "))
        itemCaption = ActionOption.User_Preferences.ToString
        actionsComboBox.Items.Add(itemCaption.Replace("_", " "))
        '    actionsComboBox.Enabled = False
        '    actionsComboBox.SelectedIndex = 0
        '    actionsComboBox.Enabled = True

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary>Closes the form and exits the application.</summary>
    Private Sub exitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitButton.Click

        ' validate controls manually.
        If Not Me.ValidateControls() Then
            ' indicate that the modal dialog is still running
            Me.DialogResult = Windows.Forms.DialogResult.None
        Else
            Me.Close()
        End If

    End Sub

    ''' <summary>Open selected items.</summary>
    Private Sub openButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openButton.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Select Case actionsComboBox.Text.Replace(" ", "_")
            Case ActionOption.User_Preferences.ToString
                Using dialog As New isr.Configuration.Testers.UserPreferences.TestPanel
                    dialog.ShowDialog()
                End Using
            Case ActionOption.Application_Configuration.ToString
                Using dialog As New isr.Configuration.Testers.AppConfig.TestPanel
                    dialog.ShowDialog()
                End Using
            Case ActionOption.INI_Settings.ToString
                Using dialog As New isr.Configuration.Testers.IniSettings.TestPanel
                    dialog.ShowDialog()
                End Using
            Case ActionOption.Profile.ToString
                Using dialog As New isr.Configuration.Testers.Profile.TestPanel
                    dialog.ShowDialog()
                End Using
            Case Else
        End Select
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

#End Region

#Region " UNUSED "

    ''' <summary>Check if program is registered.  If not, display the registration button</summary>
    ''' 'TO_DO: replace with XML signature.
    Private Sub checkPermit()

#If False Then
		' check if we have a registered version
		If goisrcRegistry.blnIsLicensed Then
		actionsComboBox.Visible = True
		Else
		actionsComboBox.Visible = False
		With cmdRegister
		.Top = actionsComboBox.Top
		.Visible = True
		End With
		End If
#Else
        actionsComboBox.Visible = True
#End If

    End Sub

#If False Then

  Private WithEvents moisrcPW As isrPassword.isrcPassword

  ''' <summary>Handles a password change and saves the new password in the registry</summary>
  ''' <param name="newPassword">The new password</param>
  Private Sub moisrcPW_Change(ByVal newPassword As String) Handles moisrcPW.Change

    ' Save the password
    ' goisrcRegistry.strPassword = newPassword

  End Sub

      moisrcPW = Nothing

#End If

#End Region

End Class