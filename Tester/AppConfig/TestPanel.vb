Namespace AppConfig
    ''' <summary>Includes code for form TestPanel.</summary>
    ''' <remarks>Launch this form by calling its Show or ShowDialog method from its default
    '''   instance.</remarks>
    ''' <license>
    ''' (c) 2003 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="09/09/03" by="David" revision="1.0.1341.x">
    '''  Created
    ''' </history>
    Friend Class TestPanel
        Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

        Public Sub New()
            MyBase.New()

            ' This method is required by the Windows Form Designer.
            InitializeComponent()

        End Sub

        'Form overrides dispose to clean up the component list.
        Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
            Try

                If disposing Then

                    If cm IsNot Nothing Then
                        cm.Dispose()
                        cm = Nothing
                    End If
                    If components IsNot Nothing Then
                        components.Dispose()
                    End If


                End If

            Finally

                ' Invoke the base class dispose method
                MyBase.Dispose(disposing)

            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        Private WithEvents exitButton As System.Windows.Forms.Button
        Private WithEvents afterLabel As System.Windows.Forms.Label
        Private WithEvents afterTextBox As System.Windows.Forms.TextBox
        Private WithEvents beforeTextBox As System.Windows.Forms.TextBox
        Private WithEvents savePendingChangesButton As System.Windows.Forms.Button
        Private WithEvents hasPendingChangesButton As System.Windows.Forms.Button
        Private WithEvents simpleExampleButton As System.Windows.Forms.Button
        Private WithEvents addDeleteModifyButton As System.Windows.Forms.Button
        Private WithEvents _StatusStrip As System.Windows.Forms.StatusStrip
        Private WithEvents _ValueOneToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
        Private WithEvents _ValueTwoToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
        Private WithEvents _ValueThreeToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
        Private WithEvents _ValueFourToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
        Private WithEvents beforeLabel As System.Windows.Forms.Label
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.exitButton = New System.Windows.Forms.Button()
            Me.afterLabel = New System.Windows.Forms.Label()
            Me.beforeLabel = New System.Windows.Forms.Label()
            Me.afterTextBox = New System.Windows.Forms.TextBox()
            Me.beforeTextBox = New System.Windows.Forms.TextBox()
            Me.savePendingChangesButton = New System.Windows.Forms.Button()
            Me.hasPendingChangesButton = New System.Windows.Forms.Button()
            Me.simpleExampleButton = New System.Windows.Forms.Button()
            Me.addDeleteModifyButton = New System.Windows.Forms.Button()
            Me._StatusStrip = New System.Windows.Forms.StatusStrip()
            Me._ValueOneToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
            Me._ValueTwoToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
            Me._ValueThreeToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
            Me._ValueFourToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
            Me._StatusStrip.SuspendLayout()
            Me.SuspendLayout()
            '
            'exitButton
            '
            Me.exitButton.Location = New System.Drawing.Point(624, 32)
            Me.exitButton.Name = "exitButton"
            Me.exitButton.Size = New System.Drawing.Size(112, 32)
            Me.exitButton.TabIndex = 1
            Me.exitButton.Text = "E&xit"
            '
            'afterLabel
            '
            Me.afterLabel.Location = New System.Drawing.Point(404, 135)
            Me.afterLabel.Name = "afterLabel"
            Me.afterLabel.Size = New System.Drawing.Size(192, 16)
            Me.afterLabel.TabIndex = 17
            Me.afterLabel.Text = "After..."
            '
            'beforeLabel
            '
            Me.beforeLabel.Location = New System.Drawing.Point(36, 135)
            Me.beforeLabel.Name = "beforeLabel"
            Me.beforeLabel.Size = New System.Drawing.Size(192, 16)
            Me.beforeLabel.TabIndex = 16
            Me.beforeLabel.Text = "Before..."
            '
            'afterTextBox
            '
            Me.afterTextBox.Location = New System.Drawing.Point(404, 159)
            Me.afterTextBox.Multiline = True
            Me.afterTextBox.Name = "afterTextBox"
            Me.afterTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.afterTextBox.Size = New System.Drawing.Size(344, 304)
            Me.afterTextBox.TabIndex = 14
            '
            'beforeTextBox
            '
            Me.beforeTextBox.Location = New System.Drawing.Point(36, 159)
            Me.beforeTextBox.Multiline = True
            Me.beforeTextBox.Name = "beforeTextBox"
            Me.beforeTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.beforeTextBox.Size = New System.Drawing.Size(344, 304)
            Me.beforeTextBox.TabIndex = 13
            '
            'savePendingChangesButton
            '
            Me.savePendingChangesButton.Location = New System.Drawing.Point(308, 79)
            Me.savePendingChangesButton.Name = "savePendingChangesButton"
            Me.savePendingChangesButton.Size = New System.Drawing.Size(232, 32)
            Me.savePendingChangesButton.TabIndex = 12
            Me.savePendingChangesButton.Text = "Save Pending Changes"
            '
            'hasPendingChangesButton
            '
            Me.hasPendingChangesButton.Location = New System.Drawing.Point(308, 31)
            Me.hasPendingChangesButton.Name = "hasPendingChangesButton"
            Me.hasPendingChangesButton.Size = New System.Drawing.Size(232, 32)
            Me.hasPendingChangesButton.TabIndex = 11
            Me.hasPendingChangesButton.Text = "Are Changes Pending?"
            '
            'simpleExampleButton
            '
            Me.simpleExampleButton.Location = New System.Drawing.Point(36, 31)
            Me.simpleExampleButton.Name = "simpleExampleButton"
            Me.simpleExampleButton.Size = New System.Drawing.Size(232, 32)
            Me.simpleExampleButton.TabIndex = 10
            Me.simpleExampleButton.Text = "Simple Example"
            '
            'addDeleteModifyButton
            '
            Me.addDeleteModifyButton.Location = New System.Drawing.Point(36, 79)
            Me.addDeleteModifyButton.Name = "addDeleteModifyButton"
            Me.addDeleteModifyButton.Size = New System.Drawing.Size(232, 32)
            Me.addDeleteModifyButton.TabIndex = 9
            Me.addDeleteModifyButton.Text = "Add/Delete/Modify settings"
            '
            '_StatusStrip
            '
            Me._StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ValueOneToolStripStatusLabel, Me._ValueTwoToolStripStatusLabel, Me._ValueThreeToolStripStatusLabel, Me._ValueFourToolStripStatusLabel})
            Me._StatusStrip.Location = New System.Drawing.Point(0, 472)
            Me._StatusStrip.Name = "_StatusStrip"
            Me._StatusStrip.Size = New System.Drawing.Size(784, 22)
            Me._StatusStrip.TabIndex = 19
            Me._StatusStrip.Text = "Status Strip"
            '
            '_ValueOneToolStripStatusLabel
            '
            Me._ValueOneToolStripStatusLabel.Name = "_ValueOneToolStripStatusLabel"
            Me._ValueOneToolStripStatusLabel.Size = New System.Drawing.Size(35, 17)
            Me._ValueOneToolStripStatusLabel.Text = "<v1>"
            '
            '_ValueTwoToolStripStatusLabel
            '
            Me._ValueTwoToolStripStatusLabel.Name = "_ValueTwoToolStripStatusLabel"
            Me._ValueTwoToolStripStatusLabel.Size = New System.Drawing.Size(35, 17)
            Me._ValueTwoToolStripStatusLabel.Text = "<v2>"
            '
            '_ValueThreeToolStripStatusLabel
            '
            Me._ValueThreeToolStripStatusLabel.Name = "_ValueThreeToolStripStatusLabel"
            Me._ValueThreeToolStripStatusLabel.Size = New System.Drawing.Size(35, 17)
            Me._ValueThreeToolStripStatusLabel.Text = "<v3>"
            '
            '_ValueFourToolStripStatusLabel
            '
            Me._ValueFourToolStripStatusLabel.Name = "_ValueFourToolStripStatusLabel"
            Me._ValueFourToolStripStatusLabel.Size = New System.Drawing.Size(35, 17)
            Me._ValueFourToolStripStatusLabel.Text = "<v4>"
            '
            'TestPanel
            '
            Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
            Me.ClientSize = New System.Drawing.Size(784, 494)
            Me.Controls.Add(Me.afterLabel)
            Me.Controls.Add(Me.beforeLabel)
            Me.Controls.Add(Me.afterTextBox)
            Me.Controls.Add(Me.beforeTextBox)
            Me.Controls.Add(Me.savePendingChangesButton)
            Me.Controls.Add(Me.hasPendingChangesButton)
            Me.Controls.Add(Me.simpleExampleButton)
            Me.Controls.Add(Me.addDeleteModifyButton)
            Me.Controls.Add(Me.exitButton)
            Me.Controls.Add(Me._StatusStrip)
            Me.Name = "TestPanel"
            Me.Text = "Test Panel"
            Me._StatusStrip.ResumeLayout(False)
            Me._StatusStrip.PerformLayout()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

#End Region

#Region " CONSTRUCTORS  and  DESTRUCTORS "

#End Region

#Region " PROPERTIES "

        Private cm As isr.Configuration.AppConfigScriber
        Private Shared Function getConfigValue(ByVal key As String) As String
            Return System.Configuration.ConfigurationManager.AppSettings.Get(key)
        End Function

#End Region

#Region " FORM EVENT HANDLERS "

        ''' <summary>Occurs before the form is closed</summary>
        ''' <param name="sender"><see cref="System.Object"/> instance of this 
        '''   <see cref="System.Windows.Forms.Form"/></param>
        ''' <param name="e"><see cref="System.ComponentModel.CancelEventArgs"/></param>
        ''' <remarks>Use this method to allow canceling the closing of the form.
        '''   Because the form is not yet closed at this point, this is also the best 
        '''   place to serialize a form's visible properties, such as size and 
        '''   location.</remarks>
        Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

            ' disable the timer if any
            ' actionTimer.Enabled = False
            System.Windows.Forms.Application.DoEvents()

            ' set module objects that reference other objects to Nothing

            ' terminate form objects
            Me.terminateObjects()

        End Sub

        ''' <summary>Occurs when the form is loaded.</summary>
        ''' <param name="sender"><see cref="System.Object"/> instance of this 
        '''   <see cref="System.Windows.Forms.Form"/></param>
        ''' <param name="e"><see cref="System.EventArgs"/></param>
        ''' <remarks>Use this method for doing any final initialization right before 
        '''   the form is shown.  This is a good place to change the Visible and
        '''   ShowInTaskbar properties to start the form as hidden.  
        '''   Starting a form as hidden is useful for forms that need to be running but that
        '''   should not show themselves right away, such as forms with a notify icon in the
        '''   task bar.</remarks>
        Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            Try

                ' Turn on the form hourglass cursor
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

                ' instantiate form objects
                Me.instantiateObjects()

                ' set the form caption
                Me.Text = isr.Core.AssemblyInfoExtensions.ExtendedCaption(My.Application.Info) & ": APP CONFIG"

                ' set tool tips
                initializeUserInterface()

                ' center the form
                Me.CenterToScreen()

            Catch

                ' Use throw without an argument in order to preserve the stack location 
                ' where the exception was initially raised.
                Throw

            Finally

                ' Turn off the form hourglass cursor
                Me.Cursor = System.Windows.Forms.Cursors.Default

            End Try

        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub form_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Click

            Try

                ' Create an instance of the ISR Scriber Class
                Using appSettingsManager As isr.Configuration.AppConfigScriber = New isr.Configuration.AppConfigScriber
                    appSettingsManager.Open()

                    ' add individual values
                    appSettingsManager.Key = "v5"
                    appSettingsManager.Value = "Value 5"
                    appSettingsManager.Key = "v6"
                    appSettingsManager.Update("Value 6")

                    ' remove single values
                    appSettingsManager.Key = "v4"
                    If appSettingsManager.KeyExists() Then
                        appSettingsManager.Remove()
                    End If

                    ' add multiple values to a single key
                    appSettingsManager.Key = "MultiValue"
                    appSettingsManager.Update(New Object() {"Value 7", "Value 8", "Value 9", "Value 10", "Value 11"})

                    ' add a date value
                    appSettingsManager.Key = "DateValue"
                    appSettingsManager.SystemType = Type.GetType("system.DateTime")
                    appSettingsManager.Update(DateTime.Parse("03/20/2003 12:00:00 AM", Globalization.CultureInfo.CurrentCulture))

                    ' add an Int32 value
                    appSettingsManager.Key = "Int32"
                    appSettingsManager.Update(1001)
                End Using

            Catch ex As Exception

                If My.MyApplication.ProcessException(ex, "Unhandled exception", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            End Try

        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

        ''' <summary>Closes the form and exits the application.</summary>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub exitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitButton.Click

            Try

                Me.beforeLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                    "{0} {1}", Date.Now.Ticks, My.MyApplication.UsingDevices.ToString)
                Me.Close()

            Catch ex As isr.Configuration.BaseException

                If My.MyApplication.ProcessException(ex, "Exception occurred Closing", isr.WindowsForms.ExceptionDisplayButtons.Continue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            Catch ex As Exception

                If My.MyApplication.ProcessException(ex, "Unhandled exception", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            End Try

            Exit Sub

        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub addDeleteModifyButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles addDeleteModifyButton.Click

            Try

                If Not cm.IsOpen Then
                    cm.Open()
                End If

                ' show the XML before making changes
                TestPanel.showXml(beforeTextBox, cm.Element)

                ' add single values
                cm.Key = "v5"
                cm.Value = "Value 5"
                cm.Key = "v6"
                cm.Update("Value 6")

                ' remove single values
                cm.Key = "v4"
                If cm.KeyExists() Then
                    cm.Remove()
                End If

                ' add multiple values to a single key
                cm.Key = "MultiValue"
                cm.Update(New Object() {"Value 7", "Value 8", "Value 9", "Value 10", "Value 11"})

                ' add a date value
                cm.Key = "DateValue"
                cm.SystemType = Type.GetType("system.DateTime")
                cm.Update(DateTime.Parse("03/20/2003 12:00:00 AM", Globalization.CultureInfo.CurrentCulture))

                ' add an Int32 value
                cm.Key = "Int32"
                cm.Update(1001)

                ' show the outcome
                TestPanel.showXml(Me.afterTextBox, cm.Element)

                readValues()

            Catch ex As Exception

                If My.MyApplication.ProcessException(ex, "Unhandled exception", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            End Try

        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub ButtonSimpleExample_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles simpleExampleButton.Click

            Try

                Dim s As String = ""
                s = TestPanel.getConfigValue("v4")
                If s Is Nothing Then
                    Console.WriteLine(
                       "Adding new configuration value.")
                    Dim configFile As String =
                       Application.ExecutablePath & ".config"
                    Dim Xml As System.Xml.XmlDocument = New System.Xml.XmlDocument
                    Xml.Load(configFile)
                    Dim Element As System.Xml.XmlNode = Xml.SelectSingleNode(
                       "//configuration/appSettings")
                    TestPanel.showXml(beforeTextBox, CType(Element, System.Xml.XmlElement))
                    Dim newElement As System.Xml.XmlElement =
                       Xml.CreateElement("add")
                    newElement.SetAttribute("key", "v4")
                    newElement.SetAttribute("value", "Value 4")
                    Element.AppendChild(newElement)
                    Xml.Save(configFile)
                    ' this is not live until the app is restarted
                    Console.WriteLine(TestPanel.getConfigValue("a3"))

                    TestPanel.showXml(afterTextBox, CType(Element, System.Xml.XmlElement))

                    readValues()
                Else
                    MessageBox.Show("Configuration value " & """v4"" already exists." & "  Value: " & s)
                End If

            Catch ex As Exception

                If My.MyApplication.ProcessException(ex, "Unhandled exception", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            End Try

        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub hasPendingChangesButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hasPendingChangesButton.Click
            Try

                If Not cm.IsOpen Then
                    cm.Open()
                End If
                Dim result As String = "The Application Settings Manager "
                If cm.HasPendingChanges Then
                    result &= "has "
                Else
                    result &= "has no "
                End If
                result &= "pending changes."
                MessageBox.Show(result)

            Catch ex As Exception

                If My.MyApplication.ProcessException(ex, "Unhandled exception", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            End Try

        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub savePendingChangesButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles savePendingChangesButton.Click
            Try
                If Not cm.IsOpen Then
                    cm.Open()
                End If
                cm.Save()
                readValues()

            Catch ex As Exception

                If My.MyApplication.ProcessException(ex, "Unhandled exception", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            End Try

        End Sub

#End Region

#Region " PRIVATE  and  PROTECTED "

        ''' <summary>Reads values 1 through 4 into the status bar.</summary>
            Private Sub readValues()

            If Not cm.IsOpen Then
                cm.Open()
            End If

            ' add single values
            For i As Int32 = 1 To 4
                cm.DefaultValue = String.Empty
                cm.Key = String.Format(Globalization.CultureInfo.CurrentCulture, "v{0}", i)
                Me._StatusStrip.Items(i - 1).Text = cm.Value.ToString
            Next

        End Sub

        ''' <summary>Initializes the class objects.</summary>
        ''' <exception cref="ApplicationException" guarantee="strong">
        '''   failed instantiating objects.</exception>
        ''' <remarks>Called from the form load method to instantiate 
        '''   module-level objects.</remarks>
        Private Sub instantiateObjects()
            cm = New isr.Configuration.AppConfigScriber
        End Sub

        ''' <summary>Terminates and disposes of class-level objects.</summary>
        ''' <remarks>Called from the form Closing method.</remarks>
        Private Sub terminateObjects()
            If cm IsNot Nothing Then
                If cm.IsOpen Then
                    cm.Close()
                End If
                cm.Dispose()
            End If
            cm = Nothing
        End Sub

        ''' <summary>Initializes the user interface and tool tips.</summary>
        ''' <remarks>Call this method from the form load method to set the user interface.</remarks>
        Private Sub initializeUserInterface()
            'tipsToolTip.SetToolTip(Me.txtDuration, "Enter count-down duration in seconds")
            'tipsToolTip.SetToolTip(Me.exitButton, "Click to exit")
            readValues()
        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
        Private Shared Sub anotherManager()
            Using cm3 As isr.Configuration.AppConfigScriber = New isr.Configuration.AppConfigScriber
                With cm3
                    .Key = "NewIntegers"
                    .Remove()
                End With
            End Using
        End Sub

        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")>
        Private Shared Sub showXml(ByVal textBoxControl As TextBox, ByVal Element As System.Xml.XmlElement)
            ' show the <appSettings> element XML in "pretty print" format.
            ' Note: The "WriteTo" method includes the xml for the element itself,
            ' plus all child elements.
            Using sw As System.IO.StringWriter = New System.IO.StringWriter(Globalization.CultureInfo.CurrentCulture)
                Using writer As New System.Xml.XmlTextWriter(sw)
                    writer.Indentation = 3
                    writer.Formatting = System.Xml.Formatting.Indented
                    Element.WriteTo(writer)
                End Using
                textBoxControl.Text = sw.GetStringBuilder.ToString
            End Using
        End Sub

#End Region

    End Class

End Namespace
