Namespace Profile

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
    Partial Class TestPanel
        Inherits System.Windows.Forms.Form

        'Form overrides dispose to clean up the component list.
        <System.Diagnostics.DebuggerNonUserCode()> 
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Required by the Windows Form Designer
        Private components As System.ComponentModel.IContainer

        'NOTE: The following procedure is required by the Windows Form Designer
        'It can be modified using the Windows Form Designer.  
        'Do not modify it using the code editor.
        <System.Diagnostics.DebuggerStepThrough()> 
        Private Sub InitializeComponent()
            Me.settingNameComboBox = New System.Windows.Forms.ComboBox
            Me.defaultValueTextBox = New System.Windows.Forms.TextBox
            Me.formatTextBox = New System.Windows.Forms.TextBox
            Me._FilePathTextBox = New System.Windows.Forms.TextBox
            Me.settingTextBox = New System.Windows.Forms.TextBox
            Me.sectionTextBox = New System.Windows.Forms.TextBox
            Me.dataTypeComboBox = New System.Windows.Forms.ComboBox
            Me.defaultValueLabel = New System.Windows.Forms.Label
            Me.dataTypeLabel = New System.Windows.Forms.Label
            Me.formatLabel = New System.Windows.Forms.Label
            Me.settingNameLabel = New System.Windows.Forms.Label
            Me.sectionNameLabel = New System.Windows.Forms.Label
            Me.exitButton = New System.Windows.Forms.Button
            Me.writeSectionButton = New System.Windows.Forms.Button
            Me.readSectionButton = New System.Windows.Forms.Button
            Me.writeSettingsButton = New System.Windows.Forms.Button
            Me.readSettingsButton = New System.Windows.Forms.Button
            Me._FilePathTextBoxLabel = New System.Windows.Forms.Label
            Me.valueLabel = New System.Windows.Forms.Label
            Me.sectionLabel = New System.Windows.Forms.Label
            Me.sectionNameComboBox = New System.Windows.Forms.ComboBox
            Me.SuspendLayout()
            '
            'settingNameComboBox
            '
            Me.settingNameComboBox.Items.AddRange(New Object() {"Boolean", "Byte", "Date", "Decimal", "Double", "Int32", "Int64", "Int16", "Single", "String"})
            Me.settingNameComboBox.Location = New System.Drawing.Point(10, 121)
            Me.settingNameComboBox.Name = "settingNameComboBox"
            Me.settingNameComboBox.Size = New System.Drawing.Size(156, 21)
            Me.settingNameComboBox.TabIndex = 31
            Me.settingNameComboBox.Text = "ComboBox1"
            '
            'defaultValueTextBox
            '
            Me.defaultValueTextBox.AcceptsReturn = True
            Me.defaultValueTextBox.BackColor = System.Drawing.SystemColors.Window
            Me.defaultValueTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
            Me.defaultValueTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.defaultValueTextBox.ForeColor = System.Drawing.SystemColors.WindowText
            Me.defaultValueTextBox.Location = New System.Drawing.Point(345, 76)
            Me.defaultValueTextBox.MaxLength = 0
            Me.defaultValueTextBox.Name = "defaultValueTextBox"
            Me.defaultValueTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.defaultValueTextBox.Size = New System.Drawing.Size(238, 20)
            Me.defaultValueTextBox.TabIndex = 33
            '
            'formatTextBox
            '
            Me.formatTextBox.AcceptsReturn = True
            Me.formatTextBox.BackColor = System.Drawing.SystemColors.Window
            Me.formatTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
            Me.formatTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.formatTextBox.ForeColor = System.Drawing.SystemColors.WindowText
            Me.formatTextBox.Location = New System.Drawing.Point(178, 76)
            Me.formatTextBox.MaxLength = 0
            Me.formatTextBox.Name = "formatTextBox"
            Me.formatTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.formatTextBox.Size = New System.Drawing.Size(156, 20)
            Me.formatTextBox.TabIndex = 27
            '
            '_FilePathTextBox
            '
            Me._FilePathTextBox.AcceptsReturn = True
            Me._FilePathTextBox.BackColor = System.Drawing.SystemColors.Window
            Me._FilePathTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
            Me._FilePathTextBox.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._FilePathTextBox.ForeColor = System.Drawing.SystemColors.WindowText
            Me._FilePathTextBox.Location = New System.Drawing.Point(10, 28)
            Me._FilePathTextBox.MaxLength = 0
            Me._FilePathTextBox.Name = "_FilePathTextBox"
            Me._FilePathTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me._FilePathTextBox.Size = New System.Drawing.Size(576, 21)
            Me._FilePathTextBox.TabIndex = 23
            '
            'settingTextBox
            '
            Me.settingTextBox.AcceptsReturn = True
            Me.settingTextBox.BackColor = System.Drawing.SystemColors.Window
            Me.settingTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
            Me.settingTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.settingTextBox.ForeColor = System.Drawing.SystemColors.WindowText
            Me.settingTextBox.Location = New System.Drawing.Point(345, 122)
            Me.settingTextBox.MaxLength = 0
            Me.settingTextBox.Name = "settingTextBox"
            Me.settingTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.settingTextBox.Size = New System.Drawing.Size(238, 20)
            Me.settingTextBox.TabIndex = 35
            '
            'sectionTextBox
            '
            Me.sectionTextBox.AcceptsReturn = True
            Me.sectionTextBox.BackColor = System.Drawing.SystemColors.Window
            Me.sectionTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
            Me.sectionTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.sectionTextBox.ForeColor = System.Drawing.SystemColors.WindowText
            Me.sectionTextBox.Location = New System.Drawing.Point(10, 199)
            Me.sectionTextBox.MaxLength = 32000
            Me.sectionTextBox.Multiline = True
            Me.sectionTextBox.Name = "sectionTextBox"
            Me.sectionTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.sectionTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
            Me.sectionTextBox.Size = New System.Drawing.Size(576, 120)
            Me.sectionTextBox.TabIndex = 39
            '
            'dataTypeComboBox
            '
            Me.dataTypeComboBox.BackColor = System.Drawing.SystemColors.Window
            Me.dataTypeComboBox.Cursor = System.Windows.Forms.Cursors.Default
            Me.dataTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
            Me.dataTypeComboBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.dataTypeComboBox.ForeColor = System.Drawing.SystemColors.WindowText
            Me.dataTypeComboBox.Location = New System.Drawing.Point(178, 120)
            Me.dataTypeComboBox.Name = "dataTypeComboBox"
            Me.dataTypeComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.dataTypeComboBox.Size = New System.Drawing.Size(156, 22)
            Me.dataTypeComboBox.TabIndex = 29
            '
            'defaultValueLabel
            '
            Me.defaultValueLabel.BackColor = System.Drawing.SystemColors.Control
            Me.defaultValueLabel.Cursor = System.Windows.Forms.Cursors.Default
            Me.defaultValueLabel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.defaultValueLabel.ForeColor = System.Drawing.SystemColors.ControlText
            Me.defaultValueLabel.Location = New System.Drawing.Point(345, 60)
            Me.defaultValueLabel.Name = "defaultValueLabel"
            Me.defaultValueLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.defaultValueLabel.Size = New System.Drawing.Size(90, 16)
            Me.defaultValueLabel.TabIndex = 32
            Me.defaultValueLabel.Text = "&Default Value: "
            Me.defaultValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'dataTypeLabel
            '
            Me.dataTypeLabel.BackColor = System.Drawing.SystemColors.Control
            Me.dataTypeLabel.Cursor = System.Windows.Forms.Cursors.Default
            Me.dataTypeLabel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.dataTypeLabel.ForeColor = System.Drawing.SystemColors.ControlText
            Me.dataTypeLabel.Location = New System.Drawing.Point(178, 103)
            Me.dataTypeLabel.Name = "dataTypeLabel"
            Me.dataTypeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.dataTypeLabel.Size = New System.Drawing.Size(90, 16)
            Me.dataTypeLabel.TabIndex = 28
            Me.dataTypeLabel.Text = "Data T&ype: "
            Me.dataTypeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'formatLabel
            '
            Me.formatLabel.BackColor = System.Drawing.SystemColors.Control
            Me.formatLabel.Cursor = System.Windows.Forms.Cursors.Default
            Me.formatLabel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.formatLabel.ForeColor = System.Drawing.SystemColors.ControlText
            Me.formatLabel.Location = New System.Drawing.Point(178, 60)
            Me.formatLabel.Name = "formatLabel"
            Me.formatLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.formatLabel.Size = New System.Drawing.Size(90, 16)
            Me.formatLabel.TabIndex = 26
            Me.formatLabel.Text = "&Format: "
            Me.formatLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'settingNameLabel
            '
            Me.settingNameLabel.BackColor = System.Drawing.SystemColors.Control
            Me.settingNameLabel.Cursor = System.Windows.Forms.Cursors.Default
            Me.settingNameLabel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.settingNameLabel.ForeColor = System.Drawing.SystemColors.ControlText
            Me.settingNameLabel.Location = New System.Drawing.Point(10, 105)
            Me.settingNameLabel.Name = "settingNameLabel"
            Me.settingNameLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.settingNameLabel.Size = New System.Drawing.Size(95, 16)
            Me.settingNameLabel.TabIndex = 30
            Me.settingNameLabel.Text = "Sett&ing Name: "
            Me.settingNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'sectionNameLabel
            '
            Me.sectionNameLabel.BackColor = System.Drawing.SystemColors.Control
            Me.sectionNameLabel.Cursor = System.Windows.Forms.Cursors.Default
            Me.sectionNameLabel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.sectionNameLabel.ForeColor = System.Drawing.SystemColors.ControlText
            Me.sectionNameLabel.Location = New System.Drawing.Point(10, 60)
            Me.sectionNameLabel.Name = "sectionNameLabel"
            Me.sectionNameLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.sectionNameLabel.Size = New System.Drawing.Size(97, 16)
            Me.sectionNameLabel.TabIndex = 24
            Me.sectionNameLabel.Text = "&Section Name: "
            Me.sectionNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'exitButton
            '
            Me.exitButton.BackColor = System.Drawing.SystemColors.Control
            Me.exitButton.Cursor = System.Windows.Forms.Cursors.Default
            Me.exitButton.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.exitButton.ForeColor = System.Drawing.SystemColors.ControlText
            Me.exitButton.Location = New System.Drawing.Point(490, 333)
            Me.exitButton.Name = "exitButton"
            Me.exitButton.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.exitButton.Size = New System.Drawing.Size(97, 29)
            Me.exitButton.TabIndex = 42
            Me.exitButton.Text = "E&xit"
            Me.exitButton.UseVisualStyleBackColor = False
            '
            'writeSectionButton
            '
            Me.writeSectionButton.BackColor = System.Drawing.SystemColors.Control
            Me.writeSectionButton.Cursor = System.Windows.Forms.Cursors.Default
            Me.writeSectionButton.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.writeSectionButton.ForeColor = System.Drawing.SystemColors.ControlText
            Me.writeSectionButton.Location = New System.Drawing.Point(130, 333)
            Me.writeSectionButton.Name = "writeSectionButton"
            Me.writeSectionButton.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.writeSectionButton.Size = New System.Drawing.Size(112, 29)
            Me.writeSectionButton.TabIndex = 41
            Me.writeSectionButton.Text = "Wri&te Section"
            Me.writeSectionButton.UseVisualStyleBackColor = False
            '
            'readSectionButton
            '
            Me.readSectionButton.BackColor = System.Drawing.SystemColors.Control
            Me.readSectionButton.Cursor = System.Windows.Forms.Cursors.Default
            Me.readSectionButton.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.readSectionButton.ForeColor = System.Drawing.SystemColors.ControlText
            Me.readSectionButton.Location = New System.Drawing.Point(10, 333)
            Me.readSectionButton.Name = "readSectionButton"
            Me.readSectionButton.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.readSectionButton.Size = New System.Drawing.Size(112, 29)
            Me.readSectionButton.TabIndex = 40
            Me.readSectionButton.Text = "R&ead Section"
            Me.readSectionButton.UseVisualStyleBackColor = False
            '
            'writeSettingsButton
            '
            Me.writeSettingsButton.BackColor = System.Drawing.SystemColors.Control
            Me.writeSettingsButton.Cursor = System.Windows.Forms.Cursors.Default
            Me.writeSettingsButton.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.writeSettingsButton.ForeColor = System.Drawing.SystemColors.ControlText
            Me.writeSettingsButton.Location = New System.Drawing.Point(515, 151)
            Me.writeSettingsButton.Name = "writeSettingsButton"
            Me.writeSettingsButton.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.writeSettingsButton.Size = New System.Drawing.Size(72, 32)
            Me.writeSettingsButton.TabIndex = 37
            Me.writeSettingsButton.Text = "&Write"
            Me.writeSettingsButton.UseVisualStyleBackColor = False
            '
            'readSettingsButton
            '
            Me.readSettingsButton.BackColor = System.Drawing.SystemColors.Control
            Me.readSettingsButton.Cursor = System.Windows.Forms.Cursors.Default
            Me.readSettingsButton.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.readSettingsButton.ForeColor = System.Drawing.SystemColors.ControlText
            Me.readSettingsButton.Location = New System.Drawing.Point(429, 151)
            Me.readSettingsButton.Name = "readSettingsButton"
            Me.readSettingsButton.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.readSettingsButton.Size = New System.Drawing.Size(72, 32)
            Me.readSettingsButton.TabIndex = 36
            Me.readSettingsButton.Text = "&Read"
            Me.readSettingsButton.UseVisualStyleBackColor = False
            '
            '_FilePathTextBoxLabel
            '
            Me._FilePathTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
            Me._FilePathTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
            Me._FilePathTextBoxLabel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me._FilePathTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
            Me._FilePathTextBoxLabel.Location = New System.Drawing.Point(10, 8)
            Me._FilePathTextBoxLabel.Name = "_FilePathTextBoxLabel"
            Me._FilePathTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me._FilePathTextBoxLabel.Size = New System.Drawing.Size(90, 16)
            Me._FilePathTextBoxLabel.TabIndex = 22
            Me._FilePathTextBoxLabel.Text = "&File Name: "
            Me._FilePathTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft
            '
            'valueLabel
            '
            Me.valueLabel.BackColor = System.Drawing.SystemColors.Control
            Me.valueLabel.Cursor = System.Windows.Forms.Cursors.Default
            Me.valueLabel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.valueLabel.ForeColor = System.Drawing.SystemColors.ControlText
            Me.valueLabel.Location = New System.Drawing.Point(345, 105)
            Me.valueLabel.Name = "valueLabel"
            Me.valueLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.valueLabel.Size = New System.Drawing.Size(56, 16)
            Me.valueLabel.TabIndex = 34
            Me.valueLabel.Text = "Value: "
            Me.valueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
            '
            'sectionLabel
            '
            Me.sectionLabel.BackColor = System.Drawing.SystemColors.Control
            Me.sectionLabel.Cursor = System.Windows.Forms.Cursors.Default
            Me.sectionLabel.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
            Me.sectionLabel.ForeColor = System.Drawing.SystemColors.ControlText
            Me.sectionLabel.Location = New System.Drawing.Point(10, 181)
            Me.sectionLabel.Name = "sectionLabel"
            Me.sectionLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
            Me.sectionLabel.Size = New System.Drawing.Size(60, 16)
            Me.sectionLabel.TabIndex = 38
            Me.sectionLabel.Text = "Section:"
            '
            'sectionNameComboBox
            '
            Me.sectionNameComboBox.Items.AddRange(New Object() {"Boolean", "Byte", "Date", "Decimal", "Double", "Int32", "Int64", "Int16", "Single", "String"})
            Me.sectionNameComboBox.Location = New System.Drawing.Point(10, 75)
            Me.sectionNameComboBox.Name = "sectionNameComboBox"
            Me.sectionNameComboBox.Size = New System.Drawing.Size(156, 21)
            Me.sectionNameComboBox.TabIndex = 43
            Me.sectionNameComboBox.Text = "ComboBox1"
            '
            'TestPanel
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(599, 369)
            Me.Controls.Add(Me.sectionNameComboBox)
            Me.Controls.Add(Me.settingNameComboBox)
            Me.Controls.Add(Me.defaultValueTextBox)
            Me.Controls.Add(Me.formatTextBox)
            Me.Controls.Add(Me._FilePathTextBox)
            Me.Controls.Add(Me.settingTextBox)
            Me.Controls.Add(Me.sectionTextBox)
            Me.Controls.Add(Me.dataTypeComboBox)
            Me.Controls.Add(Me.defaultValueLabel)
            Me.Controls.Add(Me.dataTypeLabel)
            Me.Controls.Add(Me.formatLabel)
            Me.Controls.Add(Me.settingNameLabel)
            Me.Controls.Add(Me.sectionNameLabel)
            Me.Controls.Add(Me.exitButton)
            Me.Controls.Add(Me.writeSectionButton)
            Me.Controls.Add(Me.readSectionButton)
            Me.Controls.Add(Me.writeSettingsButton)
            Me.Controls.Add(Me.readSettingsButton)
            Me.Controls.Add(Me._FilePathTextBoxLabel)
            Me.Controls.Add(Me.valueLabel)
            Me.Controls.Add(Me.sectionLabel)
            Me.Name = "TestPanel"
            Me.Text = "Test Panel"
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub
        Private WithEvents settingNameComboBox As System.Windows.Forms.ComboBox
        Private WithEvents defaultValueTextBox As System.Windows.Forms.TextBox
        Private WithEvents formatTextBox As System.Windows.Forms.TextBox
        Private WithEvents _FilePathTextBox As System.Windows.Forms.TextBox
        Private WithEvents settingTextBox As System.Windows.Forms.TextBox
        Private WithEvents sectionTextBox As System.Windows.Forms.TextBox
        Private WithEvents dataTypeComboBox As System.Windows.Forms.ComboBox
        Private WithEvents defaultValueLabel As System.Windows.Forms.Label
        Private WithEvents dataTypeLabel As System.Windows.Forms.Label
        Private WithEvents formatLabel As System.Windows.Forms.Label
        Private WithEvents settingNameLabel As System.Windows.Forms.Label
        Private WithEvents sectionNameLabel As System.Windows.Forms.Label
        Private WithEvents exitButton As System.Windows.Forms.Button
        Private WithEvents writeSectionButton As System.Windows.Forms.Button
        Private WithEvents readSectionButton As System.Windows.Forms.Button
        Private WithEvents writeSettingsButton As System.Windows.Forms.Button
        Private WithEvents readSettingsButton As System.Windows.Forms.Button
        Private WithEvents _FilePathTextBoxLabel As System.Windows.Forms.Label
        Private WithEvents valueLabel As System.Windows.Forms.Label
        Private WithEvents sectionLabel As System.Windows.Forms.Label
        Private WithEvents sectionNameComboBox As System.Windows.Forms.ComboBox
    End Class

End Namespace
