Namespace Profile

    ''' <summary>
    ''' Tests the "T:ProfileScriber".</summary>
    ''' <license>
    ''' (c) 2006 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="04/20/06" by="David" revision="1.1.2301.x">
    '''  Created
    ''' </history>
    Public Class TestPanel

#Region " CONSTRUCTORS  and  DESTRUCTORS "

        Public Sub New()
            MyBase.New()

            'This call is required by the Windows Form Designer.
            InitializeComponent()
        End Sub

        ''' <summary>Initializes the class objects.</summary>
        ''' <exception cref="ApplicationException" guarantee="strong">
        '''   failed instantiating objects.</exception>
        ''' <remarks>Called from the form load method to instantiate 
        '''   module-level objects.</remarks>
        Private Sub instantiateObjects()

            ' Set the combo box data.
            dataTypeComboBox.Items.Clear()

            dataTypeComboBox.Items.Add(GetType(System.Boolean))
            dataTypeComboBox.Items.Add(GetType(System.Byte))
            dataTypeComboBox.Items.Add(GetType(System.DateTime))
            dataTypeComboBox.Items.Add(GetType(System.Decimal))
            dataTypeComboBox.Items.Add(GetType(System.Double))
            dataTypeComboBox.Items.Add(GetType(System.Int16))
            dataTypeComboBox.Items.Add(GetType(System.Int32))
            dataTypeComboBox.Items.Add(GetType(System.Int64))
            dataTypeComboBox.Items.Add(GetType(System.Single))
            dataTypeComboBox.Items.Add(GetType(System.String))
            dataTypeComboBox.SelectedIndex = 0

            ' set the default INI Settings (INI) file name
            _FilePathTextBox.Text = isr.Configuration.ProfileScriber.DefaultFilePath()

            ' Set the default section
            If My.Computer.FileSystem.FileExists(_FilePathTextBox.Text) Then
                Using ini As New isr.Configuration.ProfileScriber(_FilePathTextBox.Text)
                    Me.sectionNameComboBox.DataSource = ini.SectionCollection()
                End Using
                If Me.sectionNameComboBox.Items.Count > 0 Then
                    Me.sectionNameComboBox.SelectedIndex = 0
                End If
            Else
                Me.sectionNameComboBox.Text = "Section"
            End If

            ' Set the default setting name
            settingNameComboBox.Text = "Boolean"

            ' Set the default value
            defaultValueTextBox.Text = "0"

            ' Set the default format
            formatTextBox.Text = "0"

        End Sub

#End Region

#Region " METHODS "

        ''' <summary>Reads a setting from the INI Settings file.</summary>
        ''' <remarks>Use this method to learn how to read a setting
        '''   from the INI Settings file based on the selection on the form.
        '''   Note the difference between this method and normal operation that
        '''   will not call on using the .ConvertToType method as the type is 
        '''   not picked up from a combo box but is already known to the programmer.
        ''' </remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _read()

            ' Declare the isrSettings object
            Dim ini As isr.Configuration.ProfileScriber

            ' Create an instance of the isrSettings class
            ini = New isr.Configuration.ProfileScriber()

            Try

                ' Set the file name.  If the text data are
                ' empty, WIN.INI is used by default
                ini.FilePath = _FilePathTextBox.Text

                ' set the section name
                ini.SectionName = sectionNameComboBox.Text

                ' Set the setting name
                ini.SettingName = settingNameComboBox.Text

                ' set the action based on the data type
                Select Case Type.GetTypeCode(CType(Me.dataTypeComboBox.SelectedItem, Type))

                    Case TypeCode.Boolean

                        settingTextBox.Text = ini.ReadBoolean(Convert.ToBoolean(defaultValueTextBox.Text,
                              Globalization.CultureInfo.CurrentCulture)).ToString

                    Case TypeCode.Byte

                        settingTextBox.Text = ini.ReadByte(Convert.ToByte(defaultValueTextBox.Text,
                            Globalization.CultureInfo.CurrentCulture)).ToString

                    Case TypeCode.DateTime

                        settingTextBox.Text = ini.ReadDateTime(Convert.ToDateTime(defaultValueTextBox.Text,
                            Globalization.CultureInfo.CurrentCulture)).ToString

                    Case TypeCode.Decimal

                        settingTextBox.Text = ini.ReadDecimal(Convert.ToDecimal(defaultValueTextBox.Text,
                            Globalization.CultureInfo.CurrentCulture)).ToString

                    Case TypeCode.Double

                        settingTextBox.Text = ini.ReadDecimal(Convert.ToDecimal(defaultValueTextBox.Text,
                            Globalization.CultureInfo.CurrentCulture)).ToString

                    Case TypeCode.Int16

                        settingTextBox.Text = ini.ReadInt16(Convert.ToInt16(defaultValueTextBox.Text,
                            Globalization.CultureInfo.CurrentCulture)).ToString

                    Case TypeCode.Int32

                        settingTextBox.Text = ini.ReadInt32(Convert.ToInt32(defaultValueTextBox.Text,
                          Globalization.CultureInfo.CurrentCulture)).ToString

                    Case TypeCode.Int64

                        settingTextBox.Text = ini.ReadInt64(Convert.ToInt64(defaultValueTextBox.Text,
                          Globalization.CultureInfo.CurrentCulture)).ToString

                    Case TypeCode.Single

                        settingTextBox.Text = ini.ReadSingle(Convert.ToSingle(defaultValueTextBox.Text,
                            Globalization.CultureInfo.CurrentCulture)).ToString

                    Case TypeCode.String

                        settingTextBox.Text = ini.ReadString(defaultValueTextBox.Text)

                End Select

            Catch ex As isr.Configuration.BaseException

                ' Display the error message
                If My.MyApplication.ProcessException(ex, "Exception occurred reading setting", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            Catch ex As System.InvalidCastException

                ' Display the error message
                If My.MyApplication.ProcessException(ex, "Exception occurred reading setting", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            Catch ex As Exception

                If My.MyApplication.ProcessException(ex, "Unhandled exception", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            Finally

                ' remove the instance of the Scriber Class
                ini.Dispose()
                ini = Nothing

            End Try

        End Sub

        ''' <summary>Reads a section from the INI Settings file.</summary>
        ''' <remarks>Use this method to learn how to read a section
        '''   from the INI Settings file based. Replaces zero characters in the
        '''   section string with zeroStandInChar values before the section string 
        '''   is displayed in the text box.
        ''' </remarks>
        Private Sub readSection()

            ' Declare the isrSettings object
            Dim ini As isr.Configuration.ProfileScriber

            ' Create an instance of the isrSettings class
            ini = New isr.Configuration.ProfileScriber()

            Try

                ' set the file name.  If the text data are empty, the WIN.INI file is used
                ini.FilePath = _FilePathTextBox.Text

                ' set the section name
                ini.SectionName = sectionNameComboBox.Text

                ' declare a string collection for the section data
                Dim sectionData As System.Collections.Specialized.StringCollection = ini.SectionCollection
                Dim sectionItem As String
                Dim sectionText As New System.Text.StringBuilder
                For Each sectionItem In sectionData
                    sectionText.Append(sectionItem)
                    sectionText.Append(Environment.NewLine)
                Next
                Me.sectionTextBox.Text = sectionText.ToString

            Catch ex As isr.Configuration.BaseException

                ' Display the error message
                If My.MyApplication.ProcessException(ex, "Exception occurred reading section", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            Finally

                ' remove the instance of the INI Settings Scriber Class
                ini.Dispose()
                ini = Nothing

            End Try

        End Sub

        ''' <summary>Writes a setting to the INI Settings file.</summary>
        ''' <remarks>Use this method to learn how to write a setting
        '''   to the INI Settings file based on the selection on the form.
        ''' </remarks>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub writeSetting()

            ' Declare the isrSettings object
            Dim ini As isr.Configuration.ProfileScriber

            ' Create an instance of the isrSettings class
            ini = New isr.Configuration.ProfileScriber()

            Try

                ' set the file name.  If the text data are empty, the WIN.INI file is used
                ini.FilePath = _FilePathTextBox.Text

                ' set the section name
                ini.SectionName = sectionNameComboBox.Text

                ' set the setting name
                ini.SettingName = settingNameComboBox.Text

                ' set the action based on the data type
                Select Case Type.GetTypeCode(CType(Me.dataTypeComboBox.SelectedItem, Type))

                    Case TypeCode.Boolean

                        ini.Write(Convert.ToBoolean(settingTextBox.Text))

                    Case TypeCode.Byte

                        If Me.formatTextBox.Text.Trim.Length > 0 Then
                            ini.Write(Convert.ToByte(settingTextBox.Text), Me.formatTextBox.Text.Trim)
                        Else
                            ini.Write(Convert.ToByte(settingTextBox.Text))
                        End If

                    Case TypeCode.DateTime

                        If Me.formatTextBox.Text.Trim.Length > 0 Then
                            ini.Write(Convert.ToDateTime(settingTextBox.Text), Me.formatTextBox.Text.Trim)
                        Else
                            ini.Write(Convert.ToDateTime(settingTextBox.Text))
                        End If

                    Case TypeCode.Decimal

                        If Me.formatTextBox.Text.Trim.Length > 0 Then
                            ini.Write(Convert.ToDecimal(settingTextBox.Text), Me.formatTextBox.Text.Trim)
                        Else
                            ini.Write(Convert.ToDecimal(settingTextBox.Text))
                        End If

                    Case TypeCode.Double

                        If Me.formatTextBox.Text.Trim.Length > 0 Then
                            ini.Write(Convert.ToDouble(settingTextBox.Text), Me.formatTextBox.Text.Trim)
                        Else
                            ini.Write(Convert.ToDouble(settingTextBox.Text))
                        End If

                    Case TypeCode.Int16

                        If Me.formatTextBox.Text.Trim.Length > 0 Then
                            ini.Write(Convert.ToInt16(settingTextBox.Text), Me.formatTextBox.Text.Trim)
                        Else
                            ini.Write(Convert.ToInt16(settingTextBox.Text))
                        End If

                    Case TypeCode.Int32

                        If Me.formatTextBox.Text.Trim.Length > 0 Then
                            ini.Write(Convert.ToInt32(settingTextBox.Text), Me.formatTextBox.Text.Trim)
                        Else
                            ini.Write(Convert.ToInt32(settingTextBox.Text))
                        End If

                    Case TypeCode.Int64

                        If Me.formatTextBox.Text.Trim.Length > 0 Then
                            ini.Write(Convert.ToInt64(settingTextBox.Text), Me.formatTextBox.Text.Trim)
                        Else
                            ini.Write(Convert.ToInt64(settingTextBox.Text))
                        End If

                    Case TypeCode.Single

                        If Me.formatTextBox.Text.Trim.Length > 0 Then
                            ini.Write(Convert.ToSingle(settingTextBox.Text), Me.formatTextBox.Text.Trim)
                        Else
                            ini.Write(Convert.ToSingle(settingTextBox.Text))
                        End If

                End Select

            Catch ex As System.InvalidCastException

                ' Display the error message
                If My.MyApplication.ProcessException(ex, "Exception occurred writing setting", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            Catch ex As isr.Configuration.BaseException

                ' Display the error message
                If My.MyApplication.ProcessException(ex, "Exception occurred writing setting", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            Catch ex As Exception

                If My.MyApplication.ProcessException(ex, "Unhandled exception", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            Finally

                ' remove the instance of the INI Settings Scriber Class
                ini.Dispose()
                ini = Nothing

            End Try

        End Sub

        ''' <summary>Writes a section to the INI Settings file.</summary>
        ''' <remarks>Use this method to learn how to write a section
        '''   to the INI Settings file based. This method inserts zero characters in the
        '''   section string in place of zeroStandInChar values before the section string 
        '''   is stored.
        ''' </remarks>
        Private Sub writeSection()

            ' Declare the isrSettings object
            Dim ini As isr.Configuration.ProfileScriber

            ' Create an instance of the isrSettings class
            ini = New isr.Configuration.ProfileScriber()

            Try

                ' exit if WINI.INI is default (i.e., no file is given).
                If _FilePathTextBox.Text.Trim.Length = 0 Then

                    ' You can remove this precaution at you
                    ' own risk!
                    MessageBox.Show("This routine will not replace a section in the WIN.INI file")
                    Exit Sub

                End If

                ' set the file name.  If the text data are empty, the WIN.INI file is used
                ini.FilePath = _FilePathTextBox.Text

                ' set the section name
                ini.SectionName = sectionNameComboBox.Text & "1"

                ' Convert all alternate characters back to zero
                ' and write the section to file
                Dim sectionCollection As System.Collections.Specialized.StringCollection = New System.Collections.Specialized.StringCollection
                Dim sectionItem As String
                For Each sectionItem In sectionTextBox.Text.Split(Environment.NewLine.ToCharArray()(0))
                    sectionCollection.Add(sectionItem.TrimStart(Environment.NewLine.ToCharArray))
                Next
                ini.WriteSection(sectionCollection)

            Catch ex As isr.Configuration.BaseException

                ' Display the error message
                If My.MyApplication.ProcessException(ex, "Exception occurred writing section", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) = Windows.Forms.DialogResult.Abort Then
                    Me.Close()
                End If

            Finally

                ' remove the instance of the INI Settings Scriber Class
                ini.Dispose()
                ini = Nothing

            End Try

        End Sub

#End Region

#Region " EVENT HANDLERS "

        ''' <summary>Closes the form and exits the application.</summary>
        ''' <remarks>
        '''     Use this method to close the form and exit this application.
        ''' </remarks>
        ''' <history date="09/02/03" by="David" revision="2.0.1340.x">
        '''  Create
        ''' </history>
        Private Sub exitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitButton.Click
            Me.Close()
            Exit Sub
        End Sub

        Private Sub _FilePathTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _FilePathTextBox.Validating
            If My.Computer.FileSystem.FileExists(_FilePathTextBox.Text) Then
                Using ini As New isr.Configuration.ProfileScriber(_FilePathTextBox.Text)
                    Me.sectionNameComboBox.DataSource = ini.SectionCollection()
                End Using
                If Me.sectionNameComboBox.Items.Count > 0 Then
                    Me.sectionNameComboBox.SelectedIndex = 0
                End If
            Else
                Me.sectionNameComboBox.Text = "Section"
            End If
        End Sub

        Private Sub readSettingsButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readSettingsButton.Click
            Me._read()
        End Sub

        Private Sub readSectionButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readSectionButton.Click
            readSection()
        End Sub

        Private Sub writeSectionButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles writeSectionButton.Click
            writeSection()
        End Sub

        Private Sub writeSettingsButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles writeSettingsButton.Click
            writeSetting()
        End Sub

        ''' <summary>Occurs when the form is loaded.</summary>
        ''' <param name="sender"><see cref="System.Object"/> instance of this 
        '''   <see cref="System.Windows.Forms.Form"/></param>
        ''' <param name="e"><see cref="System.EventArgs"/></param>
        ''' <remarks>Use this method for doing any final initialization right before 
        '''   the form is shown.  This is a good place to change the Visible and
        '''   ShowInTaskbar properties to start the form as hidden.  
        '''   Starting a form as hidden is useful for forms that need to be running but that
        '''   should not show themselves right away, such as forms with a notify icon in the
        '''   task bar.</remarks>
        Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            Try

                ' Turn on the form hourglass cursor
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

                ' instantiate form objects
                Me.instantiateObjects()

                ' set the form caption
                Me.Text = isr.Core.AssemblyInfoExtensions.ExtendedCaption(My.Application.Info) & ": PROFILE TESTER"

                ' center the form
                Me.CenterToScreen()

            Catch

                ' Use throw without an argument in order to preserve the stack location 
                ' where the exception was initially raised.
                Throw

            Finally

                ' Turn off the form hourglass cursor
                Me.Cursor = System.Windows.Forms.Cursors.Default

            End Try

        End Sub

#End Region

    End Class

End Namespace
